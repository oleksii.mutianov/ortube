package com.ortube.persistence.domain.enums

enum class AnswerStatus {
    RIGHT, WRONG
}