package com.ortube.persistence.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public abstract class AbstractPlaylist {

    private String title;
    private String topic;
    private String image;
    private List<Word> words;

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    public List<Word> getWords() {
        return words;
    }

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class PlaylistFields {

        public static final String PLAYLIST_ID = "id";
        public static final String TITLE = "title";
        public static final String TOPIC = "topic";
        public static final String IMAGE = "image";
        public static final String WORDS = "words";
        public static final String PLAYLIST_CREATED_DATE = "createdDate";
        public static final String PLAYLIST_LAST_MODIFIED_DATE = "lastModifiedDate";

    }

}
