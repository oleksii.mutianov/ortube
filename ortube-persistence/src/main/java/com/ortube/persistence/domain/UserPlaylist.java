package com.ortube.persistence.domain;

import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserPlaylist extends AbstractPlaylist {

    @Id
    private String id;
    @Builder.Default
    private PlaylistPrivacy privacy = PlaylistPrivacy.PRIVATE;
    private String userId;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class UserPlaylistFields extends PlaylistFields {

        public static final String PRIVACY = "privacy";
        public static final String USER_ID = "userId";

    }

}
