package com.ortube.persistence.domain;

import com.ortube.persistence.domain.enums.UserRole;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static org.springframework.util.CollectionUtils.isEmpty;

@Document
@Data
@Builder
public class User {

    @Id
    private String id;

    @Indexed(unique = true)
    private String email;

    @Indexed(unique = true)
    private String username;

    private String password;
    private UserRole userRole;
    private String nickname;
    private String image;
    private List<RefreshToken> refreshTokens;
    private ActivationCode activationCode;

    public void addRefreshToken(RefreshToken refreshToken) {
        if (isEmpty(refreshTokens)) {
            this.refreshTokens = new ArrayList<>();
        }
        this.refreshTokens.add(refreshToken);
    }

    public List<RefreshToken> getRefreshTokens() {
        return unmodifiableList(Optional.ofNullable(refreshTokens).orElse(emptyList()));
    }
}
