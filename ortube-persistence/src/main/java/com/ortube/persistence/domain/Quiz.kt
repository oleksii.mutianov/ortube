package com.ortube.persistence.domain

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document
data class Quiz(
    val userId: String,
    val playlistId: String,
    var words: List<Word> = listOf(),
    val answers: MutableList<AnswerResult> = mutableListOf(),
    var finished: Boolean = false,
    var result: Int = 0
) {

    @Id
    lateinit var id: String

    @CreatedDate
    lateinit var startDate: LocalDateTime

    fun addResult(answerResult: AnswerResult) {
        answers.add(answerResult)
    }
}