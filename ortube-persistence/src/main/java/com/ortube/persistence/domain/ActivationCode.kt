package com.ortube.persistence.domain

import java.time.LocalDateTime

data class ActivationCode(
    val code: String,
    val expiredAt: LocalDateTime
)