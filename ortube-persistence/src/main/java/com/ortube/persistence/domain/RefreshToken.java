package com.ortube.persistence.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class RefreshToken {

    private String token;
    private LocalDateTime expiredAt;
    private String browserFingerprint;

    public boolean equalsExcludeExpiredAt(RefreshToken refreshToken) {
        return token.equals(refreshToken.token)
                && browserFingerprint.equals(refreshToken.browserFingerprint);
    }

}
