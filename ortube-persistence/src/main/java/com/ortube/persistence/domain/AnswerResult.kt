package com.ortube.persistence.domain

import com.ortube.persistence.domain.enums.AnswerStatus

data class AnswerResult(
    val word: String,
    val answerStatus: AnswerStatus,
    val userAnswers: List<String>,
    val correctOptions: MutableList<String>
)