package com.ortube.persistence.domain.enums

enum class PlaylistPrivacy {
    PUBLIC,
    PRIVATE,
    SHARED
}