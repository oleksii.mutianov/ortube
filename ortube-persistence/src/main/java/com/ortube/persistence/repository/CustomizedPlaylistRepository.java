package com.ortube.persistence.repository;

import com.ortube.persistence.domain.Playlist;

public interface CustomizedPlaylistRepository {

    Playlist update(String id, Playlist playlist);

}
