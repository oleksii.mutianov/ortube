package com.ortube.persistence.repository;

import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.repository.cust.CustomizedPlaylistSave;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserPlaylistRepository
        extends MongoRepository<UserPlaylist, String>, CustomizedUserPlaylistRepository, CustomizedPlaylistSave<UserPlaylist> {

    List<UserPlaylist> findAllByUserId(String userId, Sort sort);

    Optional<UserPlaylist> findByIdAndUserId(String playlistId, String userId);

}
