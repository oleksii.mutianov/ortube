package com.ortube.persistence.repository.impl

import com.mongodb.BasicDBObject
import com.ortube.persistence.domain.AbstractPlaylist
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_ID
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_LAST_MODIFIED_DATE
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.WORDS
import com.ortube.persistence.domain.Word
import com.ortube.persistence.domain.Word.WordFields.IMAGE
import com.ortube.persistence.domain.Word.WordFields.LANG_1
import com.ortube.persistence.domain.Word.WordFields.LANG_2
import com.ortube.persistence.domain.Word.WordFields.WORD_ID
import com.ortube.persistence.repository.WordRepository
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class WordRepositoryImpl<E : AbstractPlaylist?>(private val mongoTemplate: MongoTemplate) : WordRepository<E> {

    override fun addWord(playlistId: String, word: Word, playlistClass: Class<E>): E {
        word.id = ObjectId.get().toHexString()

        val update = Update()
                .addToSet(WORDS, word)
                .set(PLAYLIST_LAST_MODIFIED_DATE, LocalDateTime.now())

        mongoTemplate.updateFirst(query(where(PLAYLIST_ID).`is`(playlistId)), update, playlistClass)

        return mongoTemplate.findById(playlistId, playlistClass)!!
    }

    override fun update(playlistId: String, wordId: String, word: Word, playlistClass: Class<E>): E {
        val playlist = mongoTemplate.findById(playlistId, playlistClass)!!
        val update = Update().set(PLAYLIST_LAST_MODIFIED_DATE, LocalDateTime.now())

        val words = playlist.words

        for (i in words.indices) {
            if (words[i].id == wordId) {
                update
                        .set("$WORDS.$i.$LANG_1", word.lang1)
                        .set("$WORDS.$i.$LANG_2", word.lang2)
                        .set("$WORDS.$i.$IMAGE", word.image)
            }
        }

        val query = query(where(PLAYLIST_ID).`is`(playlistId))
        mongoTemplate.updateFirst(query, update, playlistClass)

        return mongoTemplate.findById(playlistId, playlistClass)!!
    }

    override fun deleteByIdAndPlaylistId(wordId: String, playlistId: String, playlistClass: Class<E>) {
        val query = query(where(PLAYLIST_ID).`is`(playlistId))
        val update = Update().pull(WORDS, BasicDBObject(WORD_ID, wordId))

        mongoTemplate.updateFirst(query, update, playlistClass)
    }

    override fun deleteAllByIdAndPlaylistId(wordIds: List<String>, playlistId: String, playlistClass: Class<E>) {
        val wordIdsToDelete = wordIds.map(::ObjectId)
        val findPlaylistQuery = query(where(PLAYLIST_ID).`is`(playlistId))
        val deleteQuery = query(where(WORD_ID).`in`(wordIdsToDelete))
        val update = Update().pull(WORDS, deleteQuery.queryObject)

        mongoTemplate.updateMulti(findPlaylistQuery, update, playlistClass)
    }

}