package com.ortube.persistence.repository;

import com.ortube.persistence.domain.UserPlaylist;

public interface CustomizedUserPlaylistRepository {

    UserPlaylist update(String playlistId, UserPlaylist userPlaylist, String userId);

}
