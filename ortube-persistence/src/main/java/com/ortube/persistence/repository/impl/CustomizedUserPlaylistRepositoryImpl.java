package com.ortube.persistence.repository.impl;

import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.repository.CustomizedUserPlaylistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.IMAGE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_ID;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_LAST_MODIFIED_DATE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TITLE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TOPIC;
import static com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.PRIVACY;
import static com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.USER_ID;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
@RequiredArgsConstructor
public class CustomizedUserPlaylistRepositoryImpl implements CustomizedUserPlaylistRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public UserPlaylist update(String playlistId, UserPlaylist userPlaylist, String userId) {

        Update update = new Update()
                .set(TITLE, userPlaylist.getTitle())
                .set(PRIVACY, userPlaylist.getPrivacy())
                .set(TOPIC, userPlaylist.getTopic())
                .set(IMAGE, userPlaylist.getImage())
                .set(PLAYLIST_LAST_MODIFIED_DATE, LocalDateTime.now());

        mongoTemplate.updateFirst(query(where(PLAYLIST_ID).is(playlistId)), update, UserPlaylist.class);

        Query searchQuery = query(where(PLAYLIST_ID).is(playlistId).and(USER_ID).is(userId));
        return mongoTemplate.findOne(searchQuery, UserPlaylist.class);
    }

}
