package com.ortube.persistence.repository.cust;

import com.ortube.persistence.domain.AbstractPlaylist;
import com.ortube.persistence.domain.Word;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.Objects.nonNull;

@Repository
@RequiredArgsConstructor
public class CustomizedPlaylistSaveImpl implements CustomizedPlaylistSave<AbstractPlaylist> {

    private final MongoTemplate mongoTemplate;

    @Override
    public <S extends AbstractPlaylist> S save(S playlist) {
        setWordIds(playlist);
        return mongoTemplate.save(playlist);
    }

    private void setWordIds(AbstractPlaylist playlist) {
        List<Word> words = playlist.getWords();
        if (nonNull(words)) {
            words.forEach(word -> word.setId(ObjectId.get().toHexString()));
        }
    }

}
