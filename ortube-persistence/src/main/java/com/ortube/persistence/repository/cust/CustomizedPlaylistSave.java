package com.ortube.persistence.repository.cust;

public interface CustomizedPlaylistSave<T> {
    <S extends T> S save(S playlist);
}
