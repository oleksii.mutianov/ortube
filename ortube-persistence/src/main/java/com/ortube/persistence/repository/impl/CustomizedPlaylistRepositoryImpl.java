package com.ortube.persistence.repository.impl;

import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.repository.CustomizedPlaylistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.IMAGE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_ID;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_LAST_MODIFIED_DATE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TITLE;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TOPIC;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@RequiredArgsConstructor
@Repository
public class CustomizedPlaylistRepositoryImpl implements CustomizedPlaylistRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public Playlist update(String id, Playlist playlist) {

        Update update = new Update()
                .set(TITLE, playlist.getTitle())
                .set(TOPIC, playlist.getTopic())
                .set(IMAGE, playlist.getImage())
                .set(PLAYLIST_LAST_MODIFIED_DATE, LocalDateTime.now());

        mongoTemplate.updateFirst(query(where(PLAYLIST_ID).is(id)), update, Playlist.class);

        return mongoTemplate.findById(id, Playlist.class);
    }

}
