package com.ortube.persistence.repository;

import com.ortube.persistence.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByEmailOrUsername(String email, String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    @SuppressWarnings({"MethodName", "PMD.MethodNamingConventions"})
    Optional<User> findByActivationCode_Code(String code);

}
