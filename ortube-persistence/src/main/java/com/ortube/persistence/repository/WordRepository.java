package com.ortube.persistence.repository;

import com.ortube.persistence.domain.AbstractPlaylist;
import com.ortube.persistence.domain.Word;

import java.util.List;

public interface WordRepository<E extends AbstractPlaylist> {

    E addWord(String id, Word word, Class<E> playlistClass);

    E update(String playlistId, String wordId, Word word, Class<E> playlistClass);

    void deleteByIdAndPlaylistId(String wordId, String playlistId, Class<E> playlistClass);

    void deleteAllByIdAndPlaylistId(List<String> wordIds, String playlistId, Class<E> playlistClass);

}
