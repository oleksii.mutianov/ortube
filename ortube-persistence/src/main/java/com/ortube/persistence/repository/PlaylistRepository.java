package com.ortube.persistence.repository;

import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.repository.cust.CustomizedPlaylistSave;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlaylistRepository
        extends MongoRepository<Playlist, String>, CustomizedPlaylistRepository, CustomizedPlaylistSave<Playlist> {
}
