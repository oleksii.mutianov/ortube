package com.ortube.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@ComponentScan(basePackages = "com.ortube")
@EnableAutoConfiguration
@EnableMongoRepositories(basePackages = "com.ortube.persistence.repository")
@EnableMongoAuditing
public class DomainTestConfig {

}
