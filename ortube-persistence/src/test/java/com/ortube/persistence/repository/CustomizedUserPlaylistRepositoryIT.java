package com.ortube.persistence.repository;

import com.ortube.config.AbstractDomainIntegrationTest;
import com.ortube.datagenerators.UserPlaylistDataGenerator;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CustomizedUserPlaylistRepositoryIT extends AbstractDomainIntegrationTest {

    @Autowired
    private UserPlaylistRepository customizedUserPlaylistRepository;

    @Autowired
    private UserPlaylistDataGenerator userPlaylistDataGenerator;

    @Test
    void shouldGenerateIdsForWordsInPlaylistWhileSaving() {
        // GIVEN
        UserPlaylist userPlaylist = userPlaylistDataGenerator.userPlaylist();

        // WHEN
        UserPlaylist savedPlaylist = customizedUserPlaylistRepository.save(userPlaylist);

        // THEN
        savedPlaylist.getWords().forEach(word -> assertNotNull(word.getId()));
    }

    @Test
    void shouldUpdateAllNewFields() {

        // GIVEN
        UserPlaylist userPlaylist = userPlaylistDataGenerator.userPlaylist();

        String newTitle = randomAlphabetic(30);
        String newTopic = randomAlphabetic(30);
        PlaylistPrivacy newPrivacy = PlaylistPrivacy.PRIVATE;

        UserPlaylist updates = UserPlaylist.builder()
                .privacy(newPrivacy)
                .title(newTitle)
                .topic(newTopic)
                .build();

        // WHEN
        UserPlaylist updatedPlaylist = customizedUserPlaylistRepository.update(
                userPlaylist.getId(),
                updates,
                userPlaylist.getUserId()
        );

        // THEN
        assertEquals(newTitle, updatedPlaylist.getTitle());
        assertEquals(newTopic, updatedPlaylist.getTopic());
        assertEquals(newPrivacy.name(), updatedPlaylist.getPrivacy().name());

        assertEquals(userPlaylist.getId(), updatedPlaylist.getId());
        assertEquals(userPlaylist.getUserId(), updatedPlaylist.getUserId());
        assertEquals(userPlaylist.getWords(), updatedPlaylist.getWords());
        assertThat(updatedPlaylist.getCreatedDate()).isEqualToIgnoringNanos(userPlaylist.getCreatedDate());

        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(userPlaylist.getLastModifiedDate());

    }

}
