package com.ortube.persistence.repository;

import com.ortube.config.AbstractDomainIntegrationTest;
import com.ortube.datagenerators.PlaylistDataGenerator;
import com.ortube.persistence.domain.Playlist;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CustomizedPlaylistRepositoryIT extends AbstractDomainIntegrationTest {

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private PlaylistDataGenerator playlistDataGenerator;

    @Test
    void shouldGenerateIdsForWordsInPlaylistWhileSaving() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();

        // WHEN
        Playlist savedPlaylist = playlistRepository.save(playlist);

        // THEN
        savedPlaylist.getWords().forEach(word -> assertNotNull(word.getId()));
    }

    @Test
    void shouldUpdatePlaylist() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();
        Playlist playlistUpdates = playlistDataGenerator.playlist();

        // WHEN
        Playlist updatedPlaylist = playlistRepository.update(playlist.getId(), playlistUpdates);

        // THEN
        assertEquals(playlist.getId(), updatedPlaylist.getId());
        assertEquals(playlistUpdates.getTitle(), updatedPlaylist.getTitle());
        assertEquals(playlistUpdates.getTopic(), updatedPlaylist.getTopic());
        assertEquals(playlist.getWords(), updatedPlaylist.getWords());
        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(playlist.getLastModifiedDate());
    }

}
