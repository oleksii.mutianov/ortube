package com.ortube.persistence.repository;

import com.ortube.config.AbstractDomainIntegrationTest;
import com.ortube.datagenerators.PlaylistDataGenerator;
import com.ortube.datagenerators.WordDataGenerator;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.Word;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.ortube.TestExtensionsKt.deepClone;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class WordRepositoryWithPlaylistIT extends AbstractDomainIntegrationTest {

    @Autowired
    private WordRepository<Playlist> wordRepository;

    @Autowired
    private PlaylistDataGenerator playlistDataGenerator;

    @Autowired
    private WordDataGenerator wordDataGenerator;

    @Test
    void shouldAddWordToPlaylist() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        originalPlaylist.setWords(null);
        mongoTemplate.save(originalPlaylist);

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.addWord(originalPlaylist.getId(), deepClone(newWord), Playlist.class);
        Word actualWord = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class).getWords().get(0);

        // THEN
        assertNotNull(actualWord.getId());
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
    }

    @Test
    void shouldUpdatePlaylistLastModifiedDateAfterAddingWord() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.addWord(originalPlaylist.getId(), newWord, Playlist.class);
        Playlist updatedPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertNotEquals(originalPlaylist.getLastModifiedDate(), updatedPlaylist.getLastModifiedDate());
        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(originalPlaylist.getLastModifiedDate());
    }

    @Test
    void shouldUpdateWord() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();
        Word originalWord = playlist.getWords().get(0);

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.update(playlist.getId(), originalWord.getId(), newWord, Playlist.class);

        Word actualWord = mongoTemplate.findById(playlist.getId(), Playlist.class)
                .getWords().stream().filter(word -> word.getId().equals(originalWord.getId())).findFirst().get();

        // THEN
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
        assertNotEquals(originalWord.getLang1(), actualWord.getLang1());
        assertNotEquals(originalWord.getLang2(), actualWord.getLang2());
        assertEquals(originalWord.getId(), actualWord.getId());
    }

    @Test
    void shouldUpdatePlaylistLastModifiedDateAfterUpdatingWord() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.update(originalPlaylist.getId(), originalPlaylist.getWords().get(0).getId(), newWord, Playlist.class);

        Playlist updatedPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertNotEquals(originalPlaylist.getLastModifiedDate(), updatedPlaylist.getLastModifiedDate());
        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(originalPlaylist.getLastModifiedDate());
    }

    @Test
    void shouldDeleteWordByIdAndPlaylistId() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        Word originalWord = originalPlaylist.getWords().get(0);

        // WHEN
        wordRepository.deleteByIdAndPlaylistId(originalWord.getId(), originalPlaylist.getId(), Playlist.class);
        Playlist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 1);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord);
    }

    @Test
    void shouldDeleteAllWordsByIdsAndPlaylistId() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        Word originalWord1 = originalPlaylist.getWords().get(0);
        Word originalWord2 = originalPlaylist.getWords().get(1);

        // WHEN
        wordRepository.deleteAllByIdAndPlaylistId(
                List.of(originalWord1.getId(), originalWord2.getId()),
                originalPlaylist.getId(), Playlist.class
        );
        Playlist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 2);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord1, originalWord2);
    }

}
