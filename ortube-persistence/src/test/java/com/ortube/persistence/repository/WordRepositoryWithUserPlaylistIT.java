package com.ortube.persistence.repository;

import com.ortube.config.AbstractDomainIntegrationTest;
import com.ortube.datagenerators.UserPlaylistDataGenerator;
import com.ortube.datagenerators.WordDataGenerator;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.Word;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.ortube.TestExtensionsKt.deepClone;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class WordRepositoryWithUserPlaylistIT extends AbstractDomainIntegrationTest {

    @Autowired
    private WordRepository<UserPlaylist> wordRepository;

    @Autowired
    private UserPlaylistDataGenerator playlistDataGenerator;

    @Autowired
    private WordDataGenerator wordDataGenerator;

    @Test
    void shouldAddWordToPlaylist() {
        // GIVEN
        UserPlaylist originalPlaylist = playlistDataGenerator.userPlaylist();
        originalPlaylist.setWords(null);
        mongoTemplate.save(originalPlaylist);

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.addWord(originalPlaylist.getId(), deepClone(newWord), UserPlaylist.class);
        Word actualWord = mongoTemplate.findById(originalPlaylist.getId(), UserPlaylist.class).getWords().get(0);

        // THEN
        assertNotNull(actualWord.getId());
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
    }

    @Test
    void shouldUpdatePlaylistLastModifiedDateAfterAddingWord() {
        // GIVEN
        UserPlaylist originalPlaylist = playlistDataGenerator.userPlaylist();
        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.addWord(originalPlaylist.getId(), newWord, UserPlaylist.class);
        UserPlaylist updatedPlaylist = mongoTemplate.findById(originalPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertNotEquals(originalPlaylist.getLastModifiedDate(), updatedPlaylist.getLastModifiedDate());
        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(originalPlaylist.getLastModifiedDate());
    }

    @Test
    void shouldUpdateWord() {
        // GIVEN
        UserPlaylist playlist = playlistDataGenerator.userPlaylist();
        Word originalWord = playlist.getWords().get(0);

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.update(playlist.getId(), originalWord.getId(), newWord, UserPlaylist.class);

        Word actualWord = mongoTemplate.findById(playlist.getId(), UserPlaylist.class)
                .getWords().stream().filter(word -> word.getId().equals(originalWord.getId())).findFirst().get();

        // THEN
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
        assertNotEquals(originalWord.getLang1(), actualWord.getLang1());
        assertNotEquals(originalWord.getLang2(), actualWord.getLang2());
        assertEquals(originalWord.getId(), actualWord.getId());
    }

    @Test
    void shouldUpdatePlaylistLastModifiedDateAfterUpdatingWord() {
        // GIVEN
        UserPlaylist originalPlaylist = playlistDataGenerator.userPlaylist();

        Word newWord = wordDataGenerator.word();

        // WHEN
        wordRepository.update(originalPlaylist.getId(), originalPlaylist.getWords().get(0).getId(), newWord, UserPlaylist.class);

        UserPlaylist updatedPlaylist = mongoTemplate.findById(originalPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertNotEquals(originalPlaylist.getLastModifiedDate(), updatedPlaylist.getLastModifiedDate());
        assertThat(updatedPlaylist.getLastModifiedDate()).isAfter(originalPlaylist.getLastModifiedDate());
    }

    @Test
    void shouldDeleteWordByIdAndPlaylistId() {
        // GIVEN
        UserPlaylist originalPlaylist = playlistDataGenerator.userPlaylist();
        Word originalWord = originalPlaylist.getWords().get(0);

        // WHEN
        wordRepository.deleteByIdAndPlaylistId(originalWord.getId(), originalPlaylist.getId(), UserPlaylist.class);
        UserPlaylist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 1);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord);
    }

    @Test
    void shouldDeleteAllWordsByIdsAndPlaylistId() {
        // GIVEN
        UserPlaylist originalPlaylist = playlistDataGenerator.userPlaylist();
        Word originalWord1 = originalPlaylist.getWords().get(0);
        Word originalWord2 = originalPlaylist.getWords().get(1);

        // WHEN
        wordRepository.deleteAllByIdAndPlaylistId(
                List.of(originalWord1.getId(), originalWord2.getId()),
                originalPlaylist.getId(), UserPlaylist.class
        );
        UserPlaylist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 2);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord1, originalWord2);
    }

}
