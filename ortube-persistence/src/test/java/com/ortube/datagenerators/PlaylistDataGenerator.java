package com.ortube.datagenerators;

import com.ortube.persistence.domain.Playlist;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@TestComponent
@Profile("integration-test")
@RequiredArgsConstructor
public class PlaylistDataGenerator {

    private final WordDataGenerator wordDataGenerator;
    private final MongoTemplate mongoTemplate;

    public Playlist playlist() {

        Playlist playlist = Playlist.builder()
                .title(randomAlphabetic(30))
                .topic(randomAlphabetic(30))
                .words(wordDataGenerator.words())
                .build();

        playlist.getWords().forEach(word -> word.setId(ObjectId.get().toHexString()));

        return mongoTemplate.save(playlist);
    }
}
