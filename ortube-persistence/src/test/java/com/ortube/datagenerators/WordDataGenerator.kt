package com.ortube.datagenerators

import com.ortube.persistence.domain.Word
import org.apache.commons.lang3.RandomStringUtils.random
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile

@TestComponent
@Profile("integration-test")
class WordDataGenerator {

    @JvmOverloads
    fun word() = Word(lang1 = randomAlphabetic(30), lang2 = random(30))

    @JvmOverloads
    fun words(size: Long = 3) = generateSequence { word() }.take(size.toInt()).toList()

}