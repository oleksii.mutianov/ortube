package com.ortube.datagenerators;

import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@TestComponent
@Profile("integration-test")
@RequiredArgsConstructor
public class UserPlaylistDataGenerator {

    private final MongoTemplate mongoTemplate;
    private final WordDataGenerator wordDataGenerator;

    public UserPlaylist userPlaylist() {
        UserPlaylist userPlaylist = UserPlaylist.builder()
                .title(randomAlphabetic(30))
                .topic(randomAlphabetic(30))
                .privacy(PlaylistPrivacy.PUBLIC)
                .userId(ObjectId.get().toHexString())
                .words(wordDataGenerator.words(5))
                .build();

        userPlaylist.getWords().forEach(word -> word.setId(ObjectId.get().toHexString()));

        return mongoTemplate.save(userPlaylist);
    }

}
