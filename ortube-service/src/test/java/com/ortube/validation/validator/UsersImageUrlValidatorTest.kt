package com.ortube.validation.validator

import com.ortube.config.AbstractUnitTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks

class UsersImageUrlValidatorTest : AbstractUnitTest() {

    @InjectMocks
    lateinit var usersImageUrlValidator: UsersImageUrlValidator

    @Test
    fun `Should return true if image url is null`() {
        // GIVEN
        val imageUrl = null

        // WHEN
        val valid = usersImageUrlValidator.isValid(imageUrl, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return true if image url is valid for jpg`() {
        // GIVEN
        val imageUrl = "http://res.cloudinary.com/ortube/image/upload/v1593357278/users/i2dxi0lxmlwynyun434q.jpg"

        // WHEN
        val valid = usersImageUrlValidator.isValid(imageUrl, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return true if image url is valid for png`() {
        // GIVEN
        val imageUrl = "http://res.cloudinary.com/ortube/image/upload/v1593357278/users/i2dxi0lxmlwynyun434q.png"

        // WHEN
        val valid = usersImageUrlValidator.isValid(imageUrl, null)

        // THEN
        assertTrue(valid)
    }

    @Test
    fun `Should return false if image url is invalid`() {
        // GIVEN
        val imageUrl = "http://res.cloudinary.com/"

        // WHEN
        val valid = usersImageUrlValidator.isValid(imageUrl, null)

        // THEN
        assertFalse(valid)
    }

}