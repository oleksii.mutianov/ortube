package com.ortube.validation.validator;

import com.ortube.config.AbstractUnitTest;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

class UniqueEmailValidatorTest extends AbstractUnitTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UniqueEmailValidator uniqueEmailValidator;

    @Test
    void shouldReturnTrueIfUserByEmailNotFound() {
        // GIVEN
        var email = "email@gmail.com";
        given(userRepository.findByEmail(email)).willReturn(Optional.empty());

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(email, null);

        // THEN
        assertTrue(valid);
    }

    @Test
    void shouldReturnFalseIfUserByEmailFound() {
        // GIVEN
        var email = "email@gmail.com";
        given(userRepository.findByEmail(email)).willReturn(Optional.of(User.builder().email(email).build()));

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(email, null);

        // THEN
        assertFalse(valid);
    }
}