package com.ortube.validation.validator;

import com.ortube.config.AbstractUnitTest;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

class UniqueUsernameValidatorTest extends AbstractUnitTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UniqueUsernameValidator uniqueUsernameValidator;

    @Test
    void shouldReturnTrueIfUserByEmailNotFound() {
        // GIVEN
        var username = "username";
        given(userRepository.findByUsername(username)).willReturn(Optional.empty());

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertTrue(valid);
    }

    @Test
    void shouldReturnFalseIfUserByEmailFound() {
        // GIVEN
        var username = "username";
        given(userRepository.findByUsername(username)).willReturn(Optional.of(User.builder().username(username).build()));

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertFalse(valid);
    }
}