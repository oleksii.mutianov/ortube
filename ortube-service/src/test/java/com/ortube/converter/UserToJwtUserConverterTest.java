package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.UserToJwtUserDtoConverter;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.persistence.domain.User;
import com.ortube.security.model.JwtUser;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserToJwtUserConverterTest extends AbstractUnitTest {

    @Spy
    private UserToJwtUserDtoConverter userToJwtUserDtoConverter;

    @Spy
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldConvertUserToJwtUserDto() {
        // GIVEN
        User user = mockUserDataGenerator.user();

        // WHEN
        JwtUser jwtUser = userToJwtUserDtoConverter.convert(user);

        // THEN
        assertEquals(user.getId(), jwtUser.getId());
        assertEquals(user.getEmail(), jwtUser.getEmail());
        assertEquals(user.getUsername(), jwtUser.getUsername());
        assertEquals(user.getNickname(), jwtUser.getNickname());
        assertEquals(user.getUserRole().name(), jwtUser.getAuthorities().stream().findFirst().get().getAuthority());
    }

}
