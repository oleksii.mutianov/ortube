package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.RequestDtoToUserPlaylistConverter;
import com.ortube.datagenerators.MockUserPlaylistDataGenerator;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RequestDtoToUserPlaylistConverterTest extends AbstractUnitTest {

    @Spy
    private RequestDtoToUserPlaylistConverter requestDtoToUserPlaylistConverter;

    @Spy
    private MockUserPlaylistDataGenerator mockUserPlaylistDataGenerator;

    @Test
    void shouldConvertRequestDtoToUserPlaylist() {
        // GIVEN
        UserPlaylistRequestDto userPlaylistRequestDto = mockUserPlaylistDataGenerator.userPlaylistRequestDto();

        // WHEN
        UserPlaylist userPlaylist = requestDtoToUserPlaylistConverter.convert(userPlaylistRequestDto);

        // THEN
        assertEquals(userPlaylistRequestDto.getTitle(), userPlaylist.getTitle());
        assertEquals(userPlaylistRequestDto.getTopic(), userPlaylist.getTopic());
        assertEquals(userPlaylistRequestDto.getPrivacy(), userPlaylist.getPrivacy());
    }
}
