package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.PlaylistToBriefDtoConverter;
import com.ortube.converters.WordToBriefDtoConverter;
import com.ortube.datagenerators.MockPlaylistDataGenerator;
import com.ortube.dto.PlaylistBriefDto;
import com.ortube.persistence.domain.Playlist;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlaylistToBriefDtoConverterTest extends AbstractUnitTest {

    @Mock
    private WordToBriefDtoConverter wordToBriefDtoConverter;

    @InjectMocks
    private PlaylistToBriefDtoConverter playlistToBriefDtoConverter;

    @Spy
    private MockPlaylistDataGenerator mockPlaylistDataGenerator;

    @Test
    void shouldConvertPlaylistToBriefDto() {

        // GIVEN
        Playlist playlist = mockPlaylistDataGenerator.playlist();

        // WHEN
        PlaylistBriefDto playlistDto = playlistToBriefDtoConverter.convert(playlist);

        // THEN
        assertEquals(playlist.getId(), playlistDto.getId());
        assertEquals(playlist.getTitle(), playlistDto.getTitle());
        assertEquals(playlist.getTopic(), playlistDto.getTopic());
        assertThat(playlistDto.getWordCount()).isEqualTo(playlist.getWords().size());
        assertThat(playlistDto.getWords()).hasSize(3);

    }

    @Test
    void shouldReturnWordCount0AndEmptyWordsWhenWordsIsNull() {

        // GIVEN
        Playlist playlist = mockPlaylistDataGenerator.playlist();
        playlist.setWords(null);

        // WHEN
        PlaylistBriefDto playlistDto = playlistToBriefDtoConverter.convert(playlist);

        // THEN
        assertEquals(playlist.getId(), playlistDto.getId());
        assertEquals(playlist.getTitle(), playlistDto.getTitle());
        assertEquals(playlist.getTopic(), playlistDto.getTopic());
        assertThat(playlistDto.getWordCount()).isEqualTo(0);
        assertThat(playlistDto.getWords()).isEmpty();

    }

}
