package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.PlaylistToUserPlaylistConverter;
import com.ortube.datagenerators.MockPlaylistDataGenerator;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlaylistToUserPlaylistConverterTest extends AbstractUnitTest {

    @Spy
    private PlaylistToUserPlaylistConverter playlistToUserPlaylistConverter;

    @Spy
    private MockPlaylistDataGenerator mockPlaylistDataGenerator;

    @Test
    void shouldConvertPlaylistToUserPlaylist() {
        // GIVEN
        Playlist playlist = mockPlaylistDataGenerator.playlist();

        // WHEN
        UserPlaylist userPlaylist = playlistToUserPlaylistConverter.convert(playlist);

        // THEN
        assertEquals(playlist.getTitle(), userPlaylist.getTitle());
        assertEquals(playlist.getTopic(), userPlaylist.getTopic());
        assertEquals(PlaylistPrivacy.PRIVATE, userPlaylist.getPrivacy());
        assertEquals(playlist.getWords(), userPlaylist.getWords());
    }
}
