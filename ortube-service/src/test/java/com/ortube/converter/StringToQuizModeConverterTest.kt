package com.ortube.converter

import com.ortube.config.AbstractUnitTest
import com.ortube.constants.enums.QuizMode
import com.ortube.converters.StringToQuizModeConverter
import com.ortube.exceptions.UnknownModeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks

class StringToQuizModeConverterTest : AbstractUnitTest() {

    @InjectMocks
    lateinit var converter: StringToQuizModeConverter

    @Test
    fun `Should convert string to QuizMode`() {
        // GIVEN
        val modeString = "lang1-lang2"

        // WHEN
        val mode = converter.convert(modeString)

        // THEN
        assertThat(mode).isEqualTo(QuizMode.LANG1_LANG2)
    }

    @Test
    fun `Should throw UnknownModeException if mode name is invalid`() {
        // GIVEN
        val modeString = "invalid"

        // WHEN THEN
        assertThrows(UnknownModeException::class.java) { converter.convert(modeString) }
    }
}