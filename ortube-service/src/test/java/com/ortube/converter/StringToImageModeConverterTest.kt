package com.ortube.converter

import com.ortube.config.AbstractUnitTest
import com.ortube.constants.enums.ImageMode
import com.ortube.converters.StringToImageModeConverter
import com.ortube.exceptions.UnknownModeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks

class StringToImageModeConverterTest : AbstractUnitTest() {

    @InjectMocks
    lateinit var converter: StringToImageModeConverter

    @Test
    fun `Should convert string to ImageMode`() {
        // GIVEN
        val modeString = "no-image"

        // WHEN
        val mode = converter.convert(modeString)

        // THEN
        assertThat(mode).isEqualTo(ImageMode.NO_IMAGE)
    }

    @Test
    fun `Should throw UnknownModeException if mode name is invalid`() {
        // GIVEN
        val modeString = "invalid"

        // WHEN THEN
        assertThrows(UnknownModeException::class.java) { converter.convert(modeString) }
    }
}