package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.WordToBriefDtoConverter;
import com.ortube.datagenerators.WordDataGenerator;
import com.ortube.dto.WordBriefDto;
import com.ortube.persistence.domain.Word;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WordToBriefDtoConverterTest extends AbstractUnitTest {

    @Spy
    private WordToBriefDtoConverter wordToBriefDtoConverter;

    @Spy
    private WordDataGenerator wordDataGenerator;

    @Test
    void shouldConvertWordToBriefDto() {

        // GIVEN
        Word word = wordDataGenerator.word();

        // WHEN
        WordBriefDto wordBriefDto = wordToBriefDtoConverter.convert(word);

        // THEN
        assertEquals(word.getLang1(), wordBriefDto.getLang1());
        assertEquals(word.getLang2(), wordBriefDto.getLang2());

    }

}
