package com.ortube.datagenerators;

import com.ortube.persistence.domain.Playlist;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@TestComponent
@Profile("integration-test")
public class MockPlaylistDataGenerator {

    private final WordDataGenerator wordDataGenerator = new WordDataGenerator();

    public Playlist playlist() {
        return Playlist.builder()
                .id(ObjectId.get().toHexString())
                .title(randomAlphabetic(30))
                .topic(randomAlphabetic(30))
                .words(wordDataGenerator.words())
                .build();
    }

}
