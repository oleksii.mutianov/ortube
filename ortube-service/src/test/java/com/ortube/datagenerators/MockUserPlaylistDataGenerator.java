package com.ortube.datagenerators;

import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@TestComponent
@Profile("integration-test")
public class MockUserPlaylistDataGenerator {

    private final WordDataGenerator wordDataGenerator = new WordDataGenerator();

    public UserPlaylist userPlaylist() {
        UserPlaylist userPlaylist = userPlaylist(ObjectId.get().toHexString(), PlaylistPrivacy.PRIVATE);
        userPlaylist.setId(ObjectId.get().toHexString());
        return userPlaylist;
    }

    public UserPlaylist userPlaylist(String playlistId, String userId) {
        UserPlaylist userPlaylist = userPlaylist(userId, PlaylistPrivacy.PUBLIC);
        userPlaylist.setId(playlistId);
        return userPlaylist;
    }

    public UserPlaylist userPlaylist(String userId, PlaylistPrivacy privacy) {
        return UserPlaylist.builder()
                .title(randomAlphabetic(20))
                .topic(randomAlphabetic(20))
                .privacy(privacy)
                .userId(userId)
                .image(randomAlphabetic(20))
                .words(wordDataGenerator.words(5))
                .build();
    }

    public UserPlaylistRequestDto userPlaylistRequestDto() {
        return UserPlaylistRequestDto.builder()
                .title(randomAlphabetic(20))
                .topic(randomAlphabetic(20))
                .privacy(PlaylistPrivacy.PRIVATE)
                .build();
    }

}
