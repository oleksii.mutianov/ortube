package com.ortube.datagenerators;

import com.ortube.security.jwt.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;

import java.time.LocalDateTime;

@TestComponent
@Profile("integration-test")
public class TokenDataGenerator {

    @Value("${jwt.token.secret}")
    private String tokenTestSigningKey;

    @Value("${jwt.refreshToken.expired}")
    private long tokenTestExpirationTime;

    public String jwtToken(String subject, String scope) {
        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scope", scope);
        LocalDateTime currentTime = LocalDateTime.now();
        return JwtUtils.build(claims, currentTime, tokenTestExpirationTime, tokenTestSigningKey);
    }

}
