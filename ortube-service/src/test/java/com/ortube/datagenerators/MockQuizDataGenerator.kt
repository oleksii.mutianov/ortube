package com.ortube.datagenerators

import com.ortube.persistence.domain.Quiz
import com.ortube.persistence.domain.Word
import java.time.LocalDateTime

class MockQuizDataGenerator {

    fun quiz(
        quizId: String,
        userId: String,
        playlistId: String,
        words: MutableList<Word>
    ) = Quiz(userId, playlistId, ArrayList(words)).apply {
        id = quizId
        startDate = LocalDateTime.now()
    }

}