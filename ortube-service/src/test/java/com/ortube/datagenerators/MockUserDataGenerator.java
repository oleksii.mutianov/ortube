package com.ortube.datagenerators;

import com.ortube.dto.MockUserCreateDto;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.domain.enums.UserRole;
import com.ortube.security.model.JwtUser;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@TestComponent
@Profile("integration-test")
public class MockUserDataGenerator {

    public User user() {
        return User.builder()
                .id(ObjectId.get().toHexString())
                .email(randomAlphabetic(20))
                .password(randomAlphabetic(20))
                .nickname(randomAlphabetic(20))
                .username(randomAlphabetic(20))
                .userRole(UserRole.USER)
                .build();
    }

    public JwtUser jwtUserDto() {
        return JwtUser.builder()
                .id(ObjectId.get().toHexString())
                .email(randomAlphabetic(20))
                .password(randomAlphabetic(20))
                .nickname(randomAlphabetic(20))
                .username(randomAlphabetic(20))
                .authorities(singletonList(new SimpleGrantedAuthority(UserRole.USER.name())))
                .build();
    }

    public MockUserCreateDto userCreateDto() {
        return MockUserCreateDto.builder()
                .email(randomAlphabetic(20))
                .password(randomAlphabetic(20))
                .nickName(randomAlphabetic(20))
                .userName(randomAlphabetic(20))
                .role(UserRole.USER)
                .build();
    }

}
