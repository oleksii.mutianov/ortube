package com.ortube.datagenerators;

import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@TestComponent
@Profile("integration-test")
@RequiredArgsConstructor
public class UserPlaylistDataGenerator {

    private final MongoTemplate mongoTemplate;
    private final MockUserPlaylistDataGenerator mockUserPlaylistDataGenerator;

    public UserPlaylist userPlaylist(String userId) {
        return userPlaylist(userId, PlaylistPrivacy.PUBLIC);
    }

    public UserPlaylist userPlaylist(String userId, PlaylistPrivacy privacy) {
        UserPlaylist userPlaylist = mockUserPlaylistDataGenerator.userPlaylist(userId, privacy);
        userPlaylist.getWords().forEach(word -> word.setId(ObjectId.get().toHexString()));
        return mongoTemplate.save(userPlaylist);
    }

    public List<UserPlaylist> userPlaylists(String userId, long size) {
        return Stream.generate(() -> userPlaylist(userId, PlaylistPrivacy.PUBLIC)).limit(size).collect(toList());
    }
}
