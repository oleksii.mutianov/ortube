package com.ortube.datagenerators

import com.ortube.dto.WordRequestDto
import com.ortube.persistence.domain.Word
import org.apache.commons.lang3.RandomStringUtils.random
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile

@TestComponent
@Profile("integration-test")
class WordDataGenerator {

    fun word() = Word(
            lang1 = randomAlphabetic(30),
            lang2 = random(30),
            image = randomAlphabetic(30)
    )

    @JvmOverloads
    fun wordWithId(lang1: String = randomAlphabetic(30), lang2: String = random(30)) = Word(
            id = ObjectId.get().toHexString(),
            lang1 = lang1,
            lang2 = lang2,
            image = randomAlphabetic(30)
    )

    @JvmOverloads
    fun words(size: Long = 3) = generateSequence { word() }.take(size.toInt()).toList()

    fun wordRequestDto() = WordRequestDto(
            lang1 = randomAlphabetic(30),
            lang2 = random(30),
            image = randomAlphabetic(30)
    )

}