package com.ortube.config;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import static com.icegreen.greenmail.util.ServerSetup.PROTOCOL_SMTP;

@TestConfiguration
@Profile("integration-test")
@Slf4j
public class TestSmtpServerConfig {

    @Value("${spring.mail.port}")
    private Integer port;

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    @Bean
    public GreenMail greenMail() {
        GreenMail smtpServer = new GreenMail(new ServerSetup(port, host, PROTOCOL_SMTP));
        smtpServer.setUser(username, password);
        smtpServer.start();
        return smtpServer;
    }
}
