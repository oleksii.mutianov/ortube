package com.ortube.config;

import com.cloudinary.Cloudinary;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.security.model.UserContextHolder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {GeneralTestConfig.class, TestSmtpServerConfig.class})
@ActiveProfiles("integration-test")
public abstract class AbstractIntegrationTest {

    @Autowired
    protected MongoTemplate mongoTemplate;

    @MockBean
    protected UserContextHolder userContextHolder;

    @MockBean
    protected AuthenticationManager authenticationManager;

    @MockBean
    protected Cloudinary cloudinary;

    @BeforeEach
    void clearDb() {
        mongoTemplate.dropCollection(Playlist.class);
        mongoTemplate.dropCollection(User.class);
        mongoTemplate.dropCollection(UserPlaylist.class);
    }

}
