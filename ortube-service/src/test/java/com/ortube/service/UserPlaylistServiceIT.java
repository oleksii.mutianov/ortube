package com.ortube.service;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.PlaylistDataGenerator;
import com.ortube.datagenerators.UserDataGenerator;
import com.ortube.datagenerators.UserPlaylistDataGenerator;
import com.ortube.dto.UserPlaylistBriefDto;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.dto.WordBriefDto;
import com.ortube.exceptions.PlaylistNotFoundException;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.Word;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@SuppressWarnings("PMD.TooManyMethods")
public class UserPlaylistServiceIT extends AbstractIntegrationTest {

    @Autowired
    private UserPlaylistService userPlaylistService;

    @Autowired
    private UserPlaylistDataGenerator userPlaylistDataGenerator;

    @Autowired
    private PlaylistDataGenerator playlistDataGenerator;

    @Autowired
    private UserDataGenerator userDataGenerator;

    private String userId;
    private UserPlaylist userPlaylist;

    // GIVEN
    @BeforeEach
    void mockData() {
        userId = userDataGenerator.user().getId();
        userPlaylist = userPlaylistDataGenerator.userPlaylist(userId);
        given(userContextHolder.getUserId()).willReturn(userId);
    }

    @Test
    void shouldThrowPlaylistNotFoundWhenPlaylistNotFound() {
        assertThrows(PlaylistNotFoundException.class, () -> userPlaylistService.findById("-1"));
    }

    @Test
    void shouldFindByIdAndUserId() {

        // WHEN
        UserPlaylist actualUserPlaylist = userPlaylistService.findById(userPlaylist.getId());

        // THEN
        assertEquals(userPlaylist.getId(), actualUserPlaylist.getId());
        assertEquals(userPlaylist.getTitle(), actualUserPlaylist.getTitle());
        assertEquals(userPlaylist.getTopic(), actualUserPlaylist.getTopic());
        assertEquals(userPlaylist.getPrivacy(), actualUserPlaylist.getPrivacy());
        assertThat(actualUserPlaylist.getWords()).hasSameSizeAs(userPlaylist.getWords());

        IntStream.range(0, actualUserPlaylist.getWords().size()).forEach(i -> {
            Word word = userPlaylist.getWords().get(i);
            Word actualWord = actualUserPlaylist.getWords().get(i);

            assertEquals(word.getLang1(), actualWord.getLang1());
            assertEquals(word.getLang2(), actualWord.getLang2());
        });
    }

    @Test
    void shouldFindAllByUserIdInReversedOrder() {
        userPlaylistService.delete(userPlaylist.getId()); // we should have empty UserPlaylists collection before test

        // GIVEN
        List<UserPlaylist> userPlaylists = userPlaylistDataGenerator.userPlaylists(userId, 3);

        // WHEN
        List<UserPlaylistBriefDto> userPlaylistDtos = userPlaylistService.findAllSortedByLastModified();

        // THEN
        assertThat(userPlaylistDtos).hasSameSizeAs(userPlaylists);

        IntStream.range(0, userPlaylists.size()).forEach(playlistIndex -> {
            UserPlaylist userPlaylist = userPlaylists.get(playlistIndex);
            UserPlaylistBriefDto userPlaylistDto = userPlaylistDtos.get(userPlaylists.size() - 1 - playlistIndex);

            assertEquals(userPlaylist.getId(), userPlaylistDto.getId());
            assertEquals(userPlaylist.getTitle(), userPlaylistDto.getTitle());
            assertEquals(userPlaylist.getTopic(), userPlaylistDto.getTopic());
            assertEquals(userPlaylist.getPrivacy(), userPlaylistDto.getPrivacy());
            assertThat(userPlaylistDto.getWordCount()).isEqualTo(userPlaylist.getWords().size());
            assertThat(userPlaylistDto.getWords().size()).isEqualTo(3);
            IntStream.range(0, userPlaylistDto.getWords().size()).forEach(wordIndex -> {
                Word word = userPlaylist.getWords().get(wordIndex);
                WordBriefDto wordBriefDto = userPlaylistDto.getWords().get(wordIndex);

                assertEquals(word.getLang1(), wordBriefDto.getLang1());
                assertEquals(word.getLang2(), wordBriefDto.getLang2());

            });
        });
    }

    @Test
    void shouldCreatePlaylistFromRequestDto() {
        // GIVEN
        UserPlaylistRequestDto userPlaylistRequestDto = UserPlaylistRequestDto.builder()
                .topic("topic").title("title").privacy(PlaylistPrivacy.PRIVATE).build();

        // WHEN
        UserPlaylist userPlaylist = userPlaylistService.save(userPlaylistRequestDto);
        UserPlaylist actualUserPlaylist = mongoTemplate.findById(userPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertEquals(userPlaylistRequestDto.getTitle(), actualUserPlaylist.getTitle());
        assertEquals(userPlaylistRequestDto.getTopic(), actualUserPlaylist.getTopic());
        assertEquals(userPlaylistRequestDto.getPrivacy(), actualUserPlaylist.getPrivacy());
        assertEquals(userPlaylist.getId(), actualUserPlaylist.getId());
        assertNull(actualUserPlaylist.getWords());
        assertEquals(userId, actualUserPlaylist.getUserId());
    }

    @Test
    void shouldUpdatePlaylist() {
        // GIVEN
        String newTitle = "newTitle";
        String newTopic = "newTopic";
        PlaylistPrivacy newPrivacy = PlaylistPrivacy.PRIVATE;

        UserPlaylistRequestDto userPlaylistRequestDto = UserPlaylistRequestDto.builder()
                .title(newTitle)
                .topic(newTopic)
                .privacy(newPrivacy)
                .build();

        // WHEN
        userPlaylistService.update(userPlaylist.getId(), userPlaylistRequestDto);
        UserPlaylist actualUserPlaylist = mongoTemplate.findById(userPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertEquals(userPlaylist.getId(), actualUserPlaylist.getId());
        assertEquals(newTitle, actualUserPlaylist.getTitle());
        assertEquals(newTopic, actualUserPlaylist.getTopic());
        assertEquals(newPrivacy, actualUserPlaylist.getPrivacy());
        assertEquals(userId, actualUserPlaylist.getUserId());
        assertThat(actualUserPlaylist.getWords()).isEqualTo(userPlaylist.getWords());

        assertNotEquals(userPlaylist.getTitle(), actualUserPlaylist.getTitle());
        assertNotEquals(userPlaylist.getTopic(), actualUserPlaylist.getTopic());
        assertNotEquals(userPlaylist.getPrivacy(), actualUserPlaylist.getPrivacy());

    }

    @Test
    void shouldDeletePlaylist() {

        // WHEN
        userPlaylistService.delete(userPlaylist.getId());

        // THEN
        assertNull(mongoTemplate.findById(userPlaylist.getId(), UserPlaylist.class));
    }

    @Test
    void shouldCreatePrivateUserPlaylistFromExistingPlaylist() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();

        // WHEN
        UserPlaylist userPlaylist = userPlaylistService.addPlaylist(playlist.getId());
        UserPlaylist actualUserPlaylist = mongoTemplate.findById(userPlaylist.getId(), UserPlaylist.class);

        // THEN
        assertNotNull(actualUserPlaylist.getId());
        assertEquals(playlist.getTitle(), actualUserPlaylist.getTitle());
        assertEquals(playlist.getTopic(), actualUserPlaylist.getTopic());
        assertEquals(PlaylistPrivacy.PRIVATE, actualUserPlaylist.getPrivacy());
        assertEquals(actualUserPlaylist.getWords(), actualUserPlaylist.getWords());
        assertEquals(userId, actualUserPlaylist.getUserId());

    }

    @Test
    void shouldFindAllPublicSortedByLastModified() {
        // GIVEN
        User user = userDataGenerator.user();

        List<UserPlaylist> userPlaylists = List.of(
                userPlaylistDataGenerator.userPlaylist(user.getId(), PlaylistPrivacy.PRIVATE),
                userPlaylistDataGenerator.userPlaylist(user.getId(), PlaylistPrivacy.PUBLIC),
                userPlaylistDataGenerator.userPlaylist(user.getId(), PlaylistPrivacy.PRIVATE),
                userPlaylistDataGenerator.userPlaylist(user.getId(), PlaylistPrivacy.PUBLIC)
        );

        List<UserPlaylist> publicPlaylists = userPlaylists.stream()
                .filter(userPlaylist -> userPlaylist.getPrivacy() == PlaylistPrivacy.PUBLIC)
                .collect(toList());

        // WHEN
        List<UserPlaylistBriefDto> userPlaylistDtos = userPlaylistService.findAllPublicSortedByLastModified(user.getId());

        // THEN
        assertThat(userPlaylistDtos).hasSameSizeAs(publicPlaylists);

        IntStream.range(0, publicPlaylists.size()).forEach(playlistIndex -> {
            UserPlaylist userPlaylist = publicPlaylists.get(playlistIndex);
            UserPlaylistBriefDto userPlaylistDto = userPlaylistDtos.get(publicPlaylists.size() - 1 - playlistIndex);

            assertEquals(userPlaylist.getId(), userPlaylistDto.getId());
            assertEquals(userPlaylist.getTitle(), userPlaylistDto.getTitle());
            assertEquals(userPlaylist.getTopic(), userPlaylistDto.getTopic());
            assertEquals(userPlaylist.getPrivacy(), userPlaylistDto.getPrivacy());
            assertThat(userPlaylistDto.getWordCount()).isEqualTo(userPlaylist.getWords().size());
            assertThat(userPlaylistDto.getWords().size()).isEqualTo(3);
            IntStream.range(0, userPlaylistDto.getWords().size()).forEach(wordIndex -> {
                Word word = userPlaylist.getWords().get(wordIndex);
                WordBriefDto wordBriefDto = userPlaylistDto.getWords().get(wordIndex);

                assertEquals(word.getLang1(), wordBriefDto.getLang1());
                assertEquals(word.getLang2(), wordBriefDto.getLang2());

            });
        });
    }

    @Test
    void shouldCopyPlaylistFromUser() {
        // GIVEN
        User userCopyFrom = userDataGenerator.user();

        UserPlaylist userPlaylist = userPlaylistDataGenerator.userPlaylist(userCopyFrom.getId(), PlaylistPrivacy.PUBLIC);

        // WHEN
        UserPlaylist copiedUserPlaylist =
                userPlaylistService.addPlaylistFromUser(userCopyFrom.getId(), userPlaylist.getId());

        // THEN
        assertNotEquals(userPlaylist.getId(), copiedUserPlaylist.getId());
        assertEquals(userPlaylist.getTitle(), copiedUserPlaylist.getTitle());
        assertEquals(userPlaylist.getTopic(), copiedUserPlaylist.getTopic());
        assertEquals(PlaylistPrivacy.PRIVATE, copiedUserPlaylist.getPrivacy());

        IntStream.range(0, copiedUserPlaylist.getWords().size()).forEach(wordIndex -> {
            Word word = userPlaylist.getWords().get(wordIndex);
            Word actualWord = copiedUserPlaylist.getWords().get(wordIndex);

            assertNotNull(actualWord.getId());
            assertNotEquals(word.getId(), actualWord.getId());
            assertEquals(word.getLang1(), actualWord.getLang1());
            assertEquals(word.getLang2(), actualWord.getLang2());

        });
    }

    @Test
    void shouldThrowAccessDeniedExceptionIfPlaylistNotPublic() {
        // GIVEN
        User userCopyFrom = userDataGenerator.user();

        UserPlaylist userPlaylist = userPlaylistDataGenerator.userPlaylist(userCopyFrom.getId(), PlaylistPrivacy.PRIVATE);

        // WHEN THEN
        assertThrows(AccessDeniedException.class, () ->
                userPlaylistService.addPlaylistFromUser(userCopyFrom.getId(), userPlaylist.getId()));

    }

}
