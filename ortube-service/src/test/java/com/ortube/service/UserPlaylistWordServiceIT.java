package com.ortube.service;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.UserDataGenerator;
import com.ortube.datagenerators.UserPlaylistDataGenerator;
import com.ortube.datagenerators.WordDataGenerator;
import com.ortube.dto.WordRequestDto;
import com.ortube.exceptions.WordNotFoundException;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

public class UserPlaylistWordServiceIT extends AbstractIntegrationTest {

    @Autowired
    private UserPlaylistWordService userPlaylistWordService;

    @Autowired
    private UserPlaylistDataGenerator userPlaylistDataGenerator;

    @Autowired
    private WordDataGenerator wordDataGenerator;

    @Autowired
    private UserDataGenerator userDataGenerator;

    private UserPlaylist playlist;

    // GIVEN
    @BeforeEach
    void mockData() {
        String userId = userDataGenerator.user().getId();
        playlist = userPlaylistDataGenerator.userPlaylist(userId);
        given(userContextHolder.getUserId()).willReturn(userId);
    }

    @Test
    void shouldThrowWordNotFoundOnUpdateNotExistingWord() {
        // WHEN THEN
        assertThrows(WordNotFoundException.class, () ->
                userPlaylistWordService.update(playlist.getId(), "-1", null));
    }

    @Test
    void shouldThrowWordNotFoundOnDeleteNotExistingWord() {
        // WHEN THEN
        assertThrows(WordNotFoundException.class, () ->
                userPlaylistWordService.deleteByIdAndPlaylistId("-1", playlist.getId()));
    }

    @Test
    void shouldAddWordToPlaylist() {
        // GIVEN
        WordRequestDto wordToAdd = wordDataGenerator.wordRequestDto();

        // WHEN
        UserPlaylist actualPlaylist = userPlaylistWordService.addWordToPlaylist(playlist.getId(), wordToAdd);

        List<Word> wordsMatchesWordToAdd = actualPlaylist.getWords().stream()
                .filter(word -> word.getLang1().equals(wordToAdd.getLang1())
                        && word.getLang2().equals(wordToAdd.getLang2()))
                .collect(toList());

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(playlist.getWords().size() + 1);
        assertThat(wordsMatchesWordToAdd).hasSize(1);

    }

    @Test
    void shouldUpdateWord() {
        // GIVEN
        Word originalWord = playlist.getWords().get(0);

        WordRequestDto newWord = new WordRequestDto("lang1", "lang2", "image");

        // WHEN
        userPlaylistWordService.update(playlist.getId(), originalWord.getId(), newWord);

        Word actualWord = mongoTemplate.findById(playlist.getId(), UserPlaylist.class)
                .getWords().stream().filter(word -> word.getId().equals(originalWord.getId())).findFirst().get();

        // THEN
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
        assertEquals(newWord.getImage(), actualWord.getImage());
        assertNotEquals(originalWord.getLang1(), actualWord.getLang1());
        assertNotEquals(originalWord.getLang2(), actualWord.getLang2());
        assertEquals(originalWord.getId(), actualWord.getId());
    }

    @Test
    void shouldDeleteWord() {
        // GIVEN
        Word originalWord = playlist.getWords().get(0);

        // WHEN
        userPlaylistWordService.deleteByIdAndPlaylistId(originalWord.getId(), playlist.getId());
        UserPlaylist actualPlaylist = mongoTemplate.findById(playlist.getId(), UserPlaylist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(playlist.getWords().size() - 1);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord);
    }

    @Test
    void shouldDeleteAllWordsByIdAndPlaylistId() {
        // GIVEN
        Word originalWord1 = playlist.getWords().get(0);
        Word originalWord2 = playlist.getWords().get(1);

        // WHEN
        userPlaylistWordService.deleteAllByIdAndPlaylistId(
                List.of(originalWord1.getId(), originalWord2.getId()),
                playlist.getId()
        );

        UserPlaylist actualPlaylist = mongoTemplate.findById(playlist.getId(), UserPlaylist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(playlist.getWords().size() - 2);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord1, originalWord2);
    }

}
