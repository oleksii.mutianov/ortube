package com.ortube.service

import com.ortube.config.AbstractUnitTest
import com.ortube.constants.enums.ImageMode
import com.ortube.constants.enums.QuizMode
import com.ortube.datagenerators.MockQuizDataGenerator
import com.ortube.datagenerators.MockUserPlaylistDataGenerator
import com.ortube.datagenerators.WordDataGenerator
import com.ortube.exceptions.PlaylistNotFoundException
import com.ortube.exceptions.QuizNotFoundException
import com.ortube.exceptions.TooFewWordsException
import com.ortube.exceptions.WordNotFoundException
import com.ortube.extensions.OPTIONS_TO_DILUTE_COUNT
import com.ortube.persistence.domain.AnswerResult
import com.ortube.persistence.domain.Quiz
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.AnswerStatus
import com.ortube.persistence.repository.QuizRepository
import com.ortube.persistence.repository.UserPlaylistRepository
import com.ortube.security.model.UserContextHolder
import com.ortube.service.impl.QuizServiceImpl
import com.ortube.toOptional
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.assertj.core.api.Assertions.assertThat
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.quality.Strictness

@MockitoSettings(strictness = Strictness.LENIENT)
class QuizServiceTest : AbstractUnitTest() {

    private val mockUserPlaylistDataGenerator = MockUserPlaylistDataGenerator()
    private val wordDataGenerator = WordDataGenerator()
    private val mockQuizDataGenerator = MockQuizDataGenerator()

    @Mock
    private lateinit var userContextHolder: UserContextHolder

    @Mock
    private lateinit var userPlaylistRepository: UserPlaylistRepository

    @Mock
    private lateinit var quizRepository: QuizRepository

    @InjectMocks
    private lateinit var quizService: QuizServiceImpl

    private lateinit var userPlaylist: UserPlaylist
    private lateinit var userId: String
    private lateinit var quiz: Quiz

    @BeforeEach
    fun mockData() {
        // GIVEN
        userId = ObjectId.get().toHexString()
        userPlaylist = mockUserPlaylistDataGenerator.userPlaylist(ObjectId.get().toHexString(), userId)
        quiz = mockQuizDataGenerator.quiz(ObjectId.get().toHexString(), userId, userPlaylist.id, userPlaylist.words)
        given(userContextHolder.userId).willReturn(userId)
    }

    @Test
    fun `Should throw TooFewWordsException if playlist hasn't enough words to start quiz`() {
        // GIVEN
        userPlaylist.words = listOf(wordDataGenerator.word())
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())

        // WHEN THEN
        assertThrows(TooFewWordsException::class.java) { quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, ImageMode.NO_IMAGE) }
    }

    @Test
    fun `Should throw PlaylistNotFoundException if playlistId not found when starting quiz`() {
        // GIVEN
        val invalidId = "invalidId"

        // WHEN THEN
        assertThrows(PlaylistNotFoundException::class.java) { quizService.startQuiz(invalidId, QuizMode.LANG1_LANG2, ImageMode.NO_IMAGE) }
    }

    @Test
    fun `Should return list of word lang1 titles and quizId when starting quiz in lang1-lang2 mode`() {
        // GIVEN
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        // WHEN
        val (quizId, words) = quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, ImageMode.NO_IMAGE)

        // THEN
        assertThat(quizId).isEqualTo(quiz.id)
        assertThat(words).hasSize(userPlaylist.words.size)
        assertThat(words.map { it.word }).containsAll(userPlaylist.words.map { it.lang1 })
    }

    @Test
    fun `Should return list of word with null images when starting quiz in no-image mode`() {
        // GIVEN
        val imageMode = ImageMode.NO_IMAGE
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        // WHEN
        val (_, words) = quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, imageMode)

        // THEN
        assertThat(words.map { it.image }).containsOnlyNulls()
    }

    @Test
    fun `Should return list of word with not null images when starting quiz in with-image mode`() {
        // GIVEN
        val imageMode = ImageMode.WITH_IMAGE
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        // WHEN
        val (_, words) = quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, imageMode)

        // THEN
        assertThat(words.map { it.image }).doesNotContainNull()
    }

    @Test
    fun `Should return list of word lang2 titles and quizId when starting quiz in lang2-lang1 mode`() {
        // GIVEN
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        // WHEN
        val (quizId, words) = quizService.startQuiz(userPlaylist.id, QuizMode.LANG2_LANG1, ImageMode.NO_IMAGE)

        // THEN
        assertThat(quizId).isEqualTo(quiz.id)
        assertThat(words).hasSize(userPlaylist.words.size)
        assertThat(words.map { it.word }).containsAll(userPlaylist.words.map { it.lang2 })
    }

    @Test
    fun `Should return mixed list of word lang2 and lang1 titles and quizId when starting quiz in mixed mode`() {
        // GIVEN
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        // WHEN
        val (quizId, words) = quizService.startQuiz(userPlaylist.id, QuizMode.MIXED, ImageMode.NO_IMAGE)

        // THEN
        assertThat(quizId).isEqualTo(quiz.id)
        assertThat(words).hasSize(userPlaylist.words.size)
        assertThat(words.map { it.word })
                .containsAnyElementsOf(userPlaylist.words.map { it.lang1 })
                .containsAnyElementsOf(userPlaylist.words.map { it.lang2 })
    }

    @Test
    fun `Should return only unique word titles when starting quiz`() {
        // GIVEN
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        val expectedSize = userPlaylist.words.size
        val notUniqueWord = userPlaylist.words[0]

        userPlaylist.words.add(notUniqueWord)

        // WHEN
        val (_, words) = quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, ImageMode.NO_IMAGE)

        // THEN
        assertThat(words).hasSize(expectedSize)
        assertThat(words).hasSizeLessThan(userPlaylist.words.size)
    }

    @Test
    fun `Should save quiz entity after starting quiz`() {
        // GIVEN
        given(userPlaylistRepository.findByIdAndUserId(userPlaylist.id, userId)).willReturn(userPlaylist.toOptional())
        given<Quiz>(quizRepository.save(any())).willReturn(quiz)

        val quizShouldBeSaved = Quiz(userId, userPlaylist.id, userPlaylist.words)

        // WHEN
        quizService.startQuiz(userPlaylist.id, QuizMode.LANG1_LANG2, ImageMode.NO_IMAGE)

        // THEN
        verify(quizRepository, times(1)).save(quizShouldBeSaved)
    }

    @Test
    fun `Should ignore spaces, numbers and special characters when getting options`() {
        // GIVEN
        val matchingWordTitle = "exhausting pipe"
        val translation = "выхлопная труба"
        val word = wordDataGenerator.wordWithId(matchingWordTitle, translation)

        quiz.words += word
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, "123exhausting  ; pipe?_-,.")

        // THEN
        assertThat(options).hasSize(4)
        assertThat(options).contains(translation)
    }

    @Test
    fun `Should return 4 options if word has 1 translation and playlist has more than 4 words for lang1`() {
        // GIVEN
        val matchingWordTitle = "word"
        val translation = "слово"
        val word = wordDataGenerator.wordWithId(matchingWordTitle, translation)

        quiz.words += word
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, matchingWordTitle)

        // THEN
        assertThat(options).hasSize(4)
        assertThat(options).contains(translation)
        assertThat(options).containsAnyElementsOf(quiz.words.map { it.lang2 })
        assertThat(options).doesNotContainAnyElementsOf(quiz.words.map { it.lang1 })
    }

    @RepeatedTest(50)
    fun `Should return distinct 4 options`() {
        // GIVEN
        val wordTitle1 = "word"
        val translation1 = "слово"
        val translation2 = "другое слово"

        val wordTitle2 = "another word"
        val wordTitle3 = "another one word"

        val word1 = wordDataGenerator.wordWithId(wordTitle1, translation1)
        val word2 = wordDataGenerator.wordWithId(wordTitle1, translation2)
        val word3 = wordDataGenerator.wordWithId(wordTitle2, translation2)
        val word4 = wordDataGenerator.wordWithId(wordTitle3, translation2)

        quiz.words += listOf(word1, word2, word3, word4)
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, wordTitle1)

        // THEN
        assertThat(options.toSet()).hasSize(options.size)
    }

    @RepeatedTest(50)
    fun `Should return distinct 4 options 2`() {
        // GIVEN
        val wordTitle1 = "word"
        val translation1 = "слово"
        val translation2 = "другое слово"

        val wordTitle2 = "another word"
        val wordTitle3 = "another one word"

        val word1 = wordDataGenerator.wordWithId(wordTitle1, translation1)
        val word2 = wordDataGenerator.wordWithId(wordTitle1, translation2)
        val word3 = wordDataGenerator.wordWithId(wordTitle2, translation2)
        val word4 = wordDataGenerator.wordWithId(wordTitle3, translation2)

        quiz.words += listOf(word1, word2, word3, word4)
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, translation2)

        // THEN
        assertThat(options.toSet()).hasSize(options.size)
    }

    @Test
    fun `Should return 4 options if word has 1 translation and playlist has more than 4 words for lang2`() {
        // GIVEN
        val matchingWordTitle = "word"
        val translation = "слово"
        val word = wordDataGenerator.wordWithId(matchingWordTitle, translation)

        quiz.words += word
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, translation)

        // THEN
        assertThat(options).hasSize(4)
        assertThat(options).contains(matchingWordTitle)
        assertThat(options).containsAnyElementsOf(quiz.words.map { it.lang1 })
        assertThat(options).doesNotContainAnyElementsOf(quiz.words.map { it.lang2 })
    }

    @Test
    fun `Should return 4 options that contains 2 right options if word has 2 translations`() {
        // GIVEN
        val matchingWordTitle = "word"
        val translation1 = "слово"
        val translation2 = "другое слово"

        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)
        val word2 = wordDataGenerator.wordWithId(matchingWordTitle, translation2)

        quiz.words += listOf(word1, word2)

        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, matchingWordTitle)

        // THEN
        assertThat(options).hasSize(4)
        assertThat(options).contains(translation1, translation2)
    }

    @Test
    fun `Should return all right options and 2 more if word has more than 4 translations`() {
        // GIVEN
        val matchingWordTitle = "word"
        val translation1 = "слово один"
        val translation2 = "слово два"
        val translation3 = "слово три"
        val translation4 = "слово четыре"
        val translation5 = "слово пять"

        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)
        val word2 = wordDataGenerator.wordWithId(matchingWordTitle, translation2)
        val word3 = wordDataGenerator.wordWithId(matchingWordTitle, translation3)
        val word4 = wordDataGenerator.wordWithId(matchingWordTitle, translation4)
        val word5 = wordDataGenerator.wordWithId(matchingWordTitle, translation5)

        val matchingWords = listOf(word1, word2, word3, word4, word5)

        quiz.words += matchingWords

        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val options = quizService.getOptions(quiz.id, matchingWordTitle)

        // THEN
        assertThat(options).hasSize(matchingWords.size + OPTIONS_TO_DILUTE_COUNT)
        assertThat(options).contains(translation1, translation2, translation3, translation4, translation5)
    }

    @Test
    fun `Should return exactly as much options as playlist has words if playlist has less than 4 words`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation = "слово"
        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation)
        val word2 = wordDataGenerator.wordWithId()
        val word3 = wordDataGenerator.wordWithId()

        quiz.words = mutableListOf(word1, word2, word3)

        // WHEN
        val options = quizService.getOptions(quiz.id, matchingWordTitle)

        // THEN
        assertThat(options).hasSize(quiz.words.size)
        assertThat(options).containsAll(quiz.words.map { it.lang2 })
    }

    @Test
    fun `Should throw WordNotFoundException if no translation present in playlist when getting options`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())
        val invalidWordTitle = "invalid word"

        // WHEN THEN
        assertThrows(WordNotFoundException::class.java) { quizService.getOptions(quiz.id, invalidWordTitle) }
    }

    @Test
    fun `Should throw QuizNotFoundException if quiz with such id not found`() {
        // GIVEN
        val quizId = "invalidQuizId"

        // WHEN THEN
        assertThrows(QuizNotFoundException::class.java) { quizService.getOptions(quizId, "any word") }
    }

    @Test
    fun `Should throw QuizNotFoundException if quiz with such id and userId not found`() {
        // GIVEN
        val quizId = "invalidQuizId"
        val invalidWordTitle = "invalid word"
        val randomAnswer = listOf("random answer")

        // WHEN THEN
        assertThrows(QuizNotFoundException::class.java) { quizService.giveAnAnswer(quizId, invalidWordTitle, randomAnswer) }
    }

    @Test
    fun `Should throw WordNotFoundException if no translation present in playlist when giving an answer`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())
        val invalidWordTitle = "invalid word"
        val randomAnswer = listOf("random answer")

        // WHEN THEN
        assertThrows(WordNotFoundException::class.java) { quizService.giveAnAnswer(quiz.id, invalidWordTitle, randomAnswer) }
    }

    @Test
    fun `Should update quiz after giving an answer`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val word = quiz.words[0]
        val userAnswers = listOf(word.lang2)

        // WHEN
        quizService.giveAnAnswer(quiz.id, word.lang1, userAnswers)

        // THEN
        verify(quizRepository, times(1)).save(any())
    }

    @Test
    fun `AnswerResult should contain user's answers`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation1 = "слово"
        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)

        quiz.words += word1

        val userAnswers = listOf(translation1)

        // WHEN
        val (_, _, actualUserAnswers) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswers)

        // THEN
        assertThat(actualUserAnswers).isEqualTo(userAnswers)
    }

    @Test
    fun `Should ignore spaces, numbers and special characters when giving answer`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "exhausting pipe"
        val translation = "выхлопная труба"
        val word = wordDataGenerator.wordWithId(matchingWordTitle, translation)

        quiz.words += word

        val userAnswer = listOf("  выхлопная  -,?& труба123  ")

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswer)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.RIGHT)
    }

    @Test
    fun `Should return answer STATUS right when singular correct option is chosen for lang1`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation1 = "слово"
        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)

        quiz.words += word1

        val userAnswer = listOf(translation1)

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswer)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.RIGHT)
    }

    @Test
    fun `Should return answer status RIGHT when singular correct option is chosen for lang2`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val lang1 = "word"
        val lang2 = "слово"
        val word1 = wordDataGenerator.wordWithId(lang1, lang2)

        quiz.words += word1

        val userAnswer = listOf(lang1)

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, lang2, userAnswer)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.RIGHT)
    }

    @Test
    fun `Should return answer status RIGHT when all correct options are chosen`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation1 = "слово один"
        val translation2 = "слово два"

        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)
        val word2 = wordDataGenerator.wordWithId(matchingWordTitle, translation2)
        val words = listOf(word1, word2)

        quiz.words += words
        val userAnswers = words.map { it.lang2 }

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswers)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.RIGHT)
    }

    @Test
    fun `Should return answer status wrong when chosen not all correct options`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation1 = "слово один"
        val translation2 = "слово два"

        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)
        val word2 = wordDataGenerator.wordWithId(matchingWordTitle, translation2)

        quiz.words += listOf(word1, word2)

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, listOf(translation1))

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.WRONG)
    }

    @Test
    fun `Should return answer status WRONG when all answers are incorrect`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation1 = "слово один"
        val translation2 = "слово два"

        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation1)
        val word2 = wordDataGenerator.wordWithId(matchingWordTitle, translation2)

        quiz.words += listOf(word1, word2)

        // expects that first two words are don't match correct answer
        val userAnswer = quiz.words.take(2).map { it.lang2 }

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswer)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.WRONG)
    }

    @Test
    fun `Should return answer status WRONG when one answer correct and one incorrect`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        val matchingWordTitle = "word"
        val translation = "слово один"
        val word1 = wordDataGenerator.wordWithId(matchingWordTitle, translation)

        quiz.words += word1

        // expects that first word are doesn't match correct answer
        val userAnswer = listOf(translation, userPlaylist.words[0].lang2)

        // WHEN
        val (_, answerStatus) = quizService.giveAnAnswer(quiz.id, matchingWordTitle, userAnswer)

        // THEN
        assertThat(answerStatus).isEqualTo(AnswerStatus.WRONG)
    }

    @Test
    fun `Should throw TooFewWordsException when no user answers are present`() {
        // GIVEN
        val emptyList = emptyList<String>()

        // WHEN THEN
        assertThrows(TooFewWordsException::class.java) { quizService.giveAnAnswer(quiz.id, "randomWordTitle", emptyList) }
    }

    @Test
    fun `Should throw TooFewWordsException when user answers is null`() {
        // GIVEN
        val nullList: List<String>? = null

        // WHEN THEN
        assertThrows(TooFewWordsException::class.java) { quizService.giveAnAnswer(quiz.id, "randomWordTitle", nullList) }
    }

    @Test
    fun `Should calculate resultPercentage as 75 when 3 answers from 4 are right`() {
        // GIVEN
        val rightAnswer1 = AnswerResult(randomAlphabetic(10), AnswerStatus.RIGHT, emptyList(), mutableListOf())
        val rightAnswer2 = AnswerResult(randomAlphabetic(10), AnswerStatus.RIGHT, emptyList(), mutableListOf())
        val rightAnswer3 = AnswerResult(randomAlphabetic(10), AnswerStatus.RIGHT, emptyList(), mutableListOf())
        val wrongAnswer = AnswerResult(randomAlphabetic(10), AnswerStatus.WRONG, emptyList(), mutableListOf())

        quiz.answers.addAll(listOf(rightAnswer1, rightAnswer2, rightAnswer3, wrongAnswer))

        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val (resultPercentage) = quizService.finishQuiz(quiz.id)

        // THEN
        assertThat(resultPercentage).isEqualTo(75)
    }

    @Test
    fun `Should calculate resultPercentage as 0 when no answers provided`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        val (resultPercentage) = quizService.finishQuiz(quiz.id)

        // THEN
        assertThat(resultPercentage).isEqualTo(0)
    }

    @Test
    fun `Should set finished=true after finish quiz`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        quizService.finishQuiz(quiz.id)

        // THEN
        assertThat(quiz.finished).isTrue()
    }

    @Test
    fun `Should remove words from quiz after finish`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        quizService.finishQuiz(quiz.id)

        // THEN
        assertThat(quiz.words).isEmpty()
    }

    @Test
    fun `Should update quiz after finish`() {
        // GIVEN
        given(quizRepository.findByUserIdAndIdAndFinishedIsFalse(userId, quiz.id)).willReturn(quiz.toOptional())

        // WHEN
        quizService.finishQuiz(quiz.id)

        // THEN
        verify(quizRepository, times(1)).save(any())
    }
}