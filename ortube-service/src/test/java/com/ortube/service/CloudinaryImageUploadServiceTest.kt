package com.ortube.service

import com.cloudinary.Cloudinary
import com.cloudinary.Uploader
import com.ortube.config.AbstractUnitTest
import com.ortube.constants.enums.UploadFolderName
import com.ortube.exceptions.InvalidImageFormatException
import com.ortube.service.impl.CloudinaryImageUploadService
import org.apache.commons.lang3.RandomStringUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.springframework.web.multipart.MultipartFile

class CloudinaryImageUploadServiceTest : AbstractUnitTest() {

    @Mock
    private lateinit var cloudinary: Cloudinary

    @InjectMocks
    private lateinit var cloudinaryImageUploadService: CloudinaryImageUploadService

    @Test
    fun `Should throw InvalidImageFormatException if file has unsupported format`() {
        // GIVEN
        val file = mock(MultipartFile::class.java)
        given(file.originalFilename).willReturn("video.mp4")

        // WHEN THEN
        assertThrows(InvalidImageFormatException::class.java) { cloudinaryImageUploadService.upload(UploadFolderName.playlists, file) }
    }

    @Test
    fun `Should throw InvalidImageFormatException if file is null`() {
        // GIVEN
        val file = mock(MultipartFile::class.java)
        given(file.originalFilename).willReturn(null)

        // WHEN THEN
        assertThrows(InvalidImageFormatException::class.java) { cloudinaryImageUploadService.upload(UploadFolderName.playlists, file) }
    }

    @Test
    fun `Should upload valid file`() {
        // GIVEN
        val fileName = "image.jpg"
        val file = mock(MultipartFile::class.java)

        given(file.originalFilename).willReturn(fileName)
        given(file.bytes).willReturn(fileName.toByteArray())
        given(file.size).willReturn(fileName.length.toLong())

        val expectedUrl = RandomStringUtils.randomAlphabetic(10)
        val folder = UploadFolderName.playlists

        given(cloudinary.uploader()).willReturn(mock(Uploader::class.java))
        given(cloudinary.uploader().upload(file.bytes, mapOf(Pair("folder", folder.name)))).willReturn(mapOf(Pair("url", expectedUrl)))

        // WHEN
        val upload = cloudinaryImageUploadService.upload(folder, file)

        // THEN
        assertThat(upload.imageLink).isEqualTo(expectedUrl)
        assertThat(upload.folder).isEqualTo(folder)
        assertThat(upload.size).isEqualTo(fileName.length.toLong())
    }

}