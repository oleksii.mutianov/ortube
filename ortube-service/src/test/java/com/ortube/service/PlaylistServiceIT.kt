package com.ortube.service

import com.ortube.config.AbstractIntegrationTest
import com.ortube.datagenerators.PlaylistDataGenerator
import com.ortube.dto.PlaylistRequestDto
import com.ortube.exceptions.PlaylistNotFoundException
import com.ortube.persistence.domain.Playlist
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class PlaylistServiceIT : AbstractIntegrationTest() {

    @Autowired
    private lateinit var playlistService: PlaylistService

    @Autowired
    private lateinit var playlistDataGenerator: PlaylistDataGenerator

    @Test
    fun `Should throw PlaylistNotFoundException when playlist not found`() {
        assertThrows(PlaylistNotFoundException::class.java) { playlistService.findById("-1") }
    }

    @Test
    fun `Should find playlist by id`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        val actualPlaylist = playlistService.findById(playlist.id)

        // THEN
        assertEquals(playlist.id, actualPlaylist.id)
        assertEquals(playlist.title, actualPlaylist.title)
        assertEquals(playlist.topic, actualPlaylist.topic)
        assertThat(actualPlaylist.words).hasSameSizeAs(playlist.words)

        for (i in actualPlaylist.words.indices) {
            val word = playlist.words[i]
            val actualWord = actualPlaylist.words[i]
            assertEquals(word.lang1, actualWord.lang1)
            assertEquals(word.lang2, actualWord.lang2)
        }
    }

    @Test
    fun `Should find all playlists in reversed order`() {
        // GIVEN
        val playlists = listOf(playlistDataGenerator.playlist(),
                playlistDataGenerator.playlist(), playlistDataGenerator.playlist())

        // WHEN
        val playlistDtos = playlistService.findAll()

        // THEN
        assertThat(playlistDtos).hasSameSizeAs(playlists)

        for (playlistIndex in playlists.indices) {
            val playlist = playlists[playlistIndex]
            val playlistDto = playlistDtos[playlists.size - 1 - playlistIndex]

            assertEquals(playlist.id, playlistDto.id)
            assertEquals(playlist.title, playlistDto.title)
            assertEquals(playlist.topic, playlistDto.topic)
            assertThat(playlistDto.wordCount).isEqualTo(playlist.words.size)
            assertThat(playlistDto.words.size).isEqualTo(3)

            for (wordIndex in playlistDto.words.indices) {
                val word = playlist.words[wordIndex]
                val wordBriefDto = playlistDto.words[wordIndex]

                assertEquals(word.lang1, wordBriefDto.lang1)
                assertEquals(word.lang2, wordBriefDto.lang2)
            }
        }
    }

    @Test
    fun `Should create playlist from RequestDto`() {
        // GIVEN
        val playlistRequestDto = PlaylistRequestDto(title = "topic", topic = "title")

        // WHEN
        val playlist = playlistService.create(playlistRequestDto)
        val actualPlaylist = mongoTemplate.findById(playlist.id, Playlist::class.java)!!

        // THEN
        assertEquals(playlistRequestDto.title, actualPlaylist.title)
        assertEquals(playlistRequestDto.topic, actualPlaylist.topic)
        assertEquals(playlist.id, actualPlaylist.id)
        assertNull(actualPlaylist.words)
    }

    @Test
    fun `Should update playlist`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val playlistRequestDto = PlaylistRequestDto(title = "new title", topic = "new topic")

        // WHEN
        val updatedPlaylist = playlistService.updatePlaylist(playlist.id, playlistRequestDto)

        // THEN
        assertEquals(playlist.id, updatedPlaylist.id)
        assertEquals(playlistRequestDto.title, updatedPlaylist.title)
        assertEquals(playlistRequestDto.topic, updatedPlaylist.topic)
        assertNotEquals(playlist.title, updatedPlaylist.title)
        assertNotEquals(playlist.topic, updatedPlaylist.topic)
        assertThat(updatedPlaylist.words).hasSameSizeAs(playlist.words)
    }

    @Test
    fun `Should delete playlist`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        playlistService.deletePlaylist(playlist.id)

        // THEN
        assertNull(mongoTemplate.findById(playlist.id, Playlist::class.java))
    }
}