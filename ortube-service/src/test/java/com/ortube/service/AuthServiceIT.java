package com.ortube.service;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.UserDataGenerator;
import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.persistence.domain.RefreshToken;
import com.ortube.persistence.domain.User;
import com.ortube.service.impl.AuthServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthServiceIT extends AbstractIntegrationTest {

    @Autowired
    private UserDataGenerator userDataGenerator;

    @Autowired
    private AuthServiceImpl authenticationService;

    @Test
    void shouldCreateTokens() {

        // GIVEN
        User user = userDataGenerator.user();
        String fingerprint = randomAlphabetic(30);

        // WHEN
        AuthenticationResponseDto authResponse = authenticationService.createTokens(user, fingerprint);
        List<RefreshToken> refreshTokens = mongoTemplate.findById(user.getId(), User.class).getRefreshTokens();
        RefreshToken savedToken = refreshTokens.get(0);

        // THEN
        assertEquals(user.getUserRole().name(), authResponse.getRole());
        assertNotNull(authResponse.getAccessToken());
        assertNotNull(authResponse.getRefreshToken());
        assertNotNull(authResponse.getExpiredAt());

        assertThat(refreshTokens).hasSize(1);
        assertEquals(authResponse.getRefreshToken(), savedToken.getToken());
        assertEquals(fingerprint, savedToken.getBrowserFingerprint());

    }

    @Test
    void shouldRefreshTokens() {
        // GIVEN
        User user = userDataGenerator.user();
        String fingerprint = randomAlphabetic(30);
        AuthenticationResponseDto oldAuthResponse = authenticationService.createTokens(user, fingerprint);

        // WHEN
        AuthenticationResponseDto authResponse =
                authenticationService.refreshTokens(user, oldAuthResponse.getRefreshToken(), fingerprint);

        List<RefreshToken> refreshTokens = mongoTemplate.findById(user.getId(), User.class).getRefreshTokens();
        RefreshToken savedToken = refreshTokens.get(0);

        // THEN
        refreshTokens.forEach(refreshToken -> assertNotEquals(oldAuthResponse.getRefreshToken(), refreshToken));

        assertThat(refreshTokens).hasSize(1);
        assertEquals(authResponse.getRefreshToken(), savedToken.getToken());
        assertEquals(fingerprint, savedToken.getBrowserFingerprint());
    }

}
