package com.ortube.service;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.UserDataGenerator;
import com.ortube.persistence.domain.User;
import com.ortube.security.model.JwtUser;
import com.ortube.service.impl.JwtUserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JwtUserDetailsServiceIT extends AbstractIntegrationTest {

    @Autowired
    private UserDataGenerator userDataGenerator;

    @Autowired
    private JwtUserDetailsServiceImpl jwtUserDetailsService;

    @Test
    void shouldLoadByUsername() {

        // GIVEN
        User user = userDataGenerator.user();

        // WHEN
        UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(user.getUsername());

        // THEN
        assertThat(userDetails).isExactlyInstanceOf(JwtUser.class);
        JwtUser jwtUser = (JwtUser) userDetails;
        assertEquals(user.getId(), jwtUser.getId());
        assertEquals(user.getUsername(), jwtUser.getUsername());
        assertEquals(user.getNickname(), jwtUser.getNickname());
        assertEquals(user.getEmail(), jwtUser.getEmail());
        assertEquals(user.getUserRole().name(), jwtUser.getAuthorities().stream().findFirst().get().getAuthority());
    }

    @Test
    void shouldLoadByEmail() {

        // GIVEN
        User user = userDataGenerator.user();

        // WHEN
        UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(user.getEmail());

        // THEN
        assertThat(userDetails).isExactlyInstanceOf(JwtUser.class);
        JwtUser jwtUser = (JwtUser) userDetails;
        assertEquals(user.getId(), jwtUser.getId());
        assertEquals(user.getUsername(), jwtUser.getUsername());
        assertEquals(user.getNickname(), jwtUser.getNickname());
        assertEquals(user.getEmail(), jwtUser.getEmail());
        assertEquals(user.getUserRole().name(), jwtUser.getAuthorities().stream().findFirst().get().getAuthority());
    }

    @Test
    void shouldThrowUsernameNotFoundExceptionIfInvalidUsername() {
        // GIVEN
        User user = userDataGenerator.user();

        // WHEN THEN
        assertThrows(UsernameNotFoundException.class, () ->
                jwtUserDetailsService.loadUserByUsername("Invalid " + user.getUsername()));
    }

    @Test
    void shouldThrowUsernameNotFoundExceptionIfInvalidEmail() {
        // GIVEN
        User user = userDataGenerator.user();

        // WHEN THEN
        assertThrows(UsernameNotFoundException.class, () ->
                jwtUserDetailsService.loadUserByUsername("Invalid " + user.getEmail()));
    }

}
