package com.ortube.service;

import com.ortube.config.AbstractUnitTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import com.ortube.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest extends AbstractUnitTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Spy
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldInvokeUserRepositoryWhenSavingUser() {

        // GIVEN
        User user = mockUserDataGenerator.user();
        when(userRepository.save(user)).thenReturn(user);

        // WHEN
        User savedUser = userService.save(user);

        // THEN
        assertEquals(user, savedUser);
        verify(userRepository, times(1)).save(any());
    }

    @Test
    void shouldThrowUsernameNotFoundExceptionIfUserNotFound() {
        // GIVEN
        String wrongUsername = "Wrong username";
        when(userRepository.findByEmailOrUsername(wrongUsername, wrongUsername))
                .thenThrow(UsernameNotFoundException.class);

        // WHEN THEN
        assertThrows(UsernameNotFoundException.class, () ->
                userService.findByEmailOrUsername(wrongUsername));
        verify(userRepository, times(1)).findByEmailOrUsername(wrongUsername, wrongUsername);
    }

    @Test
    void shouldReturnUser() {
        // GIVEN
        User user = mockUserDataGenerator.user();
        when(userRepository.findByEmailOrUsername(user.getUsername(), user.getUsername())).thenReturn(Optional.of(user));

        // WHEN
        User foundUser = userService.findByEmailOrUsername(user.getUsername());

        // THEN
        assertEquals(user, foundUser);
        verify(userRepository, times(1)).findByEmailOrUsername(user.getUsername(), user.getUsername());

    }

}
