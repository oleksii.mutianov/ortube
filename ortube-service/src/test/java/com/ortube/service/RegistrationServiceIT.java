package com.ortube.service;

import com.icegreen.greenmail.util.GreenMail;
import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.UserDataGenerator;
import com.ortube.dto.UserCreateDto;
import com.ortube.persistence.domain.ActivationCode;
import com.ortube.persistence.domain.User;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.List;

import static com.ortube.mail.constants.EmailSubjectsKt.REGISTRATION_SUBJECT;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class RegistrationServiceIT extends AbstractIntegrationTest {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UserDataGenerator userDataGenerator;

    @Autowired
    private GreenMail smtpServer;

    @Value("${app.letterLink}")
    private String letterLink;

    @Test
    void shouldRegisterUser() {
        // GIVEN
        UserCreateDto userCreateDto = UserCreateDto.builder()
                .email(randomAlphabetic(10))
                .username(randomAlphabetic(10))
                .nickname(randomAlphabetic(10))
                .password(randomAlphabetic(10))
                .build();

        // WHEN
        registrationService.register(userCreateDto);

        User createdUser = mongoTemplate.findAll(User.class).get(0);

        // THEN
        assertNotNull(createdUser.getId());
        assertNotNull(createdUser.getActivationCode());
        assertEquals(userCreateDto.getEmail(), createdUser.getEmail());
        assertEquals(userCreateDto.getUsername(), createdUser.getUsername());
        assertEquals(userCreateDto.getNickname(), createdUser.getNickname());
        assertNotEquals(userCreateDto.getPassword(), createdUser.getPassword());
    }

    @Test
    @SneakyThrows
    void shouldSendActivationEmailWhileRegistration() {
        smtpServer.purgeEmailFromAllMailboxes();
        // GIVEN
        UserCreateDto userCreateDto = UserCreateDto.builder()
                .email(randomAlphabetic(10))
                .username(randomAlphabetic(10))
                .nickname(randomAlphabetic(10))
                .password(randomAlphabetic(10))
                .build();

        // WHEN
        registrationService.register(userCreateDto);

        User createdUser = mongoTemplate.findAll(User.class).get(0);
        MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
        MimeMessage email = receivedMessages[0];

        // THEN
        assertThat(receivedMessages).hasSize(1);
        assertEquals(REGISTRATION_SUBJECT, email.getSubject());
        assertEquals(userCreateDto.getEmail(), email.getAllRecipients()[0].toString());
        assertThat(String.valueOf(email.getContent())).contains(emailBodyAsList(createdUser));
    }

    @Test
    void shouldActivateUser() {
        // GIVEN
        String activationCode = randomAlphabetic(10);
        User user = userDataGenerator.user();
        user.setActivationCode(new ActivationCode(activationCode, LocalDateTime.now().plusSeconds(10)));
        mongoTemplate.save(user);

        // WHEN
        String s = registrationService.activateAccount(activationCode);
        User activatedUser = mongoTemplate.findById(user.getId(), User.class);

        // THEN
        assertEquals("Account successfully activated!", s);
        assertNull(activatedUser.getActivationCode());
    }

    private List<String> emailBodyAsList(User user) {
        return List.of(
                "Dear " + user.getNickname() + ",",
                "Thanks for signing up with Ortube!",
                "Click the button below to activate your account.",
                "Have fun, and don't hesitate to contact us with your feedback.",
                "The Ortube Team",
                letterLink + user.getActivationCode().getCode()
        );
    }

}
