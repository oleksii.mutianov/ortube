package com.ortube.service;

import com.icegreen.greenmail.store.FolderException;
import com.icegreen.greenmail.util.GreenMail;
import com.ortube.config.AbstractIntegrationTest;
import com.ortube.mail.data.MailDto;
import com.ortube.mail.service.impl.MailSenderImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MailSendingServiceIT extends AbstractIntegrationTest {

    @Autowired
    private MailSenderImpl mailService;

    @Autowired
    private GreenMail smtpServer;

    @Test
    void mailSendingTest() throws MessagingException, IOException, FolderException {
        // GIVEN
        smtpServer.purgeEmailFromAllMailboxes();
        String subject = randomAlphabetic(10);
        String sendTo = randomAlphabetic(10) + "@email.com";
        String body = randomAlphabetic(100);
        MailDto mailDto = new MailDto(sendTo, subject, body);

        // WHEN
        mailService.sendEmail(mailDto);

        MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
        MimeMessage email = receivedMessages[0];

        // THEN
        assertThat(receivedMessages).hasSize(1);
        assertEquals(subject, email.getSubject());
        assertEquals(sendTo, email.getAllRecipients()[0].toString());
        assertThat(String.valueOf(email.getContent())).contains(body);
    }

}
