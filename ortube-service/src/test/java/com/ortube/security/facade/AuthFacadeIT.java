package com.ortube.security.facade;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.dto.LoginRequestDto;
import com.ortube.persistence.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

public class AuthFacadeIT extends AbstractIntegrationTest {

    private static final String PASSWORD = randomAlphabetic(10);
    private static final String ENCODED_PASSWORD = new BCryptPasswordEncoder().encode(PASSWORD);
    private static final String FINGERPRINT = randomAlphabetic(30);

    @Autowired
    private AuthFacadeImpl authenticationFacade;

    @Autowired
    private MockUserDataGenerator userDataGenerator;

    private User testUser;

    @BeforeEach
    void initData() {
        testUser = userDataGenerator.user();
        testUser.setPassword(ENCODED_PASSWORD);
        mongoTemplate.save(testUser);
        given(userContextHolder.getFingerprint()).willReturn(Optional.of(FINGERPRINT));
    }

    @Test
    void shouldReturnTokens() {

        // GIVEN
        LoginRequestDto requestDto = new LoginRequestDto(testUser.getUsername(), PASSWORD);

        // WHEN
        AuthenticationResponseDto responseDto = authenticationFacade.authenticate(requestDto);

        // THEN
        assertEquals(testUser.getUserRole().name(), responseDto.getRole());
        assertNotNull(responseDto.getAccessToken());
        assertNotNull(responseDto.getRefreshToken());

    }

    @Test
    void shouldThrowBadCredentialsExceptionIfInvalidUsername() {

        // GIVEN
        LoginRequestDto requestDto = new LoginRequestDto("Invalid username", PASSWORD);

        // WHEN THEN
        assertThrows(BadCredentialsException.class, () -> authenticationFacade.authenticate(requestDto));

    }

    @Test
    void shouldThrowBadCredentialsExceptionIfInvalidPassword() {

        // GIVEN
        LoginRequestDto loginRequest = new LoginRequestDto(testUser.getUsername(), "Invalid password");
        var auth = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());

        given(authenticationManager.authenticate(auth)).willThrow(BadCredentialsException.class);

        // WHEN THEN
        assertThrows(BadCredentialsException.class, () -> authenticationFacade.authenticate(loginRequest));

    }
}
