package com.ortube.security.jwt.validator;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.TokenDataGenerator;
import com.ortube.persistence.domain.RefreshToken;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.domain.enums.UserRole;
import com.ortube.security.model.TokenScopes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;
import static com.ortube.constants.HeadersKt.BROWSER_FINGERPRINT_HEADER;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RefreshTokenValidatorIT extends AbstractIntegrationTest {

    @Autowired
    private RefreshTokenValidator refreshTokenValidator;

    @Autowired
    private TokenDataGenerator tokenDataGenerator;

    @Test
    void shouldReturnTrueIfTokenIsValid() {

        // GIVEN
        User user = User.builder()
                .username("username")
                .nickname("nickname")
                .password("password")
                .userRole(UserRole.USER)
                .build();

        String jwtToken = tokenDataGenerator.jwtToken(user.getUsername(), TokenScopes.REFRESH_TOKEN.getValue());
        String fingerprint = randomAlphabetic(10);

        user.setRefreshTokens(singletonList(RefreshToken.builder().token(jwtToken).browserFingerprint(fingerprint).build()));

        mongoTemplate.save(user);

        Map<String, String> headers = Map.of(
                AUTHENTICATION_HEADER, jwtToken,
                BROWSER_FINGERPRINT_HEADER, fingerprint
        );

        // WHEN
        boolean result = refreshTokenValidator.validateToken(headers);

        // THEN
        assertTrue(result);

    }

    @Test
    void shouldReturnFalseIfUserDoNotOwnsToken() {

        // GIVEN
        User user = User.builder()
                .username("username")
                .nickname("nickname")
                .password("password")
                .userRole(UserRole.USER)
                .build();

        String jwtToken = tokenDataGenerator.jwtToken(user.getUsername(), TokenScopes.REFRESH_TOKEN.getValue());
        String fingerprint = randomAlphabetic(10);

        user.setRefreshTokens(singletonList(RefreshToken.builder().token(jwtToken).browserFingerprint(fingerprint).build()));

        mongoTemplate.save(user);

        Map<String, String> headers = Map.of(
                AUTHENTICATION_HEADER, jwtToken,
                BROWSER_FINGERPRINT_HEADER, "wrong fingerprint"
        );

        // WHEN
        boolean result = refreshTokenValidator.validateToken(headers);

        // THEN
        assertFalse(result);

    }

}
