package com.ortube.security.jwt.extractor;

import com.ortube.config.AbstractUnitTest;
import com.ortube.exceptions.JwtAuthenticationException;
import com.ortube.security.jwt.JwtUtils;
import com.ortube.security.model.TokenScopes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.internal.util.reflection.FieldSetter;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TokenClaimsExtractorTest extends AbstractUnitTest {

    private static TokenExtractorImpl tokenClaimsExtractor = new TokenExtractorImpl();

    private static LocalDateTime expectedExpiredAt;
    private static String token;

    @BeforeAll
    static void prepareToken() throws NoSuchFieldException {

        // GIVEN
        int expTime = 2;
        LocalDateTime currentTime = LocalDateTime.now();
        expectedExpiredAt = currentTime.plusSeconds(expTime);

        Claims claims = Jwts.claims().setSubject(USER_USERNAME);
        claims.put("scope", SCOPE);

        token = JwtUtils.build(claims, currentTime, expTime, MOCK_TOKEN_SIGNING_KEY);

        FieldSetter.setField(tokenClaimsExtractor,
                tokenClaimsExtractor.getClass().getDeclaredField("tokenSigningKey"), MOCK_TOKEN_SIGNING_KEY);
    }

    @Test
    void shouldExtractSubjectFromToken() {

        // WHEN
        String actualSubject = tokenClaimsExtractor.extractSubject(token);

        // THEN
        assertEquals(USER_USERNAME, actualSubject);
    }

    @Test
    void shouldExtractScopeFromToken() {

        // WHEN
        TokenScopes actualScope = tokenClaimsExtractor.extractScope(token);

        // THEN
        assertEquals(SCOPE, actualScope.getValue());
    }

    @Test
    void shouldExtractExpiredAtFromToken() {

        // WHEN
        LocalDateTime actualExpiredAt = tokenClaimsExtractor.extractExpiredAt(token);

        // THEN
        assertThat(actualExpiredAt).isEqualToIgnoringNanos(expectedExpiredAt);
    }

    @Test
    void shouldThrowJwtAuthenticationExceptionIfTokenIsInvalid() {
        assertThrows(JwtAuthenticationException.class, () -> tokenClaimsExtractor.extractScope("invalid token"));
    }

}
