package com.ortube.security.jwt.extractor;

import com.ortube.config.AbstractUnitTest;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static com.ortube.constants.HeadersKt.HEADER_PREFIX;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenExtractorImplTest extends AbstractUnitTest {

    @Spy
    private TokenExtractorImpl tokenExtractorImpl;

    @Test
    void shouldExtractBearerTokenFromString() {

        // GIVEN
        String expectedToken = randomAlphabetic(30);
        String tokenHeader = HEADER_PREFIX + expectedToken;

        // WHEN
        String actualToken = tokenExtractorImpl.extractToken(tokenHeader);

        // THEN
        assertEquals(expectedToken, actualToken);

    }

}
