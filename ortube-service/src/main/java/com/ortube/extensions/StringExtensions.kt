package com.ortube.extensions

fun String.removeNonLetters() = this.replace(Regex("\\PL+"), "")

fun String.equalsIgnoringNonLetters(other: String): Boolean {
    return this.removeNonLetters().equals(other.removeNonLetters(), ignoreCase = true)
}