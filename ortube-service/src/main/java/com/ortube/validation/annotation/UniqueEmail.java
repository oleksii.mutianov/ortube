package com.ortube.validation.annotation;

import com.ortube.validation.validator.UniqueEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.ortube.constants.ErrorMessagesKt.EMAIL_IS_TAKEN_MSG;

@Documented
@Constraint(validatedBy = UniqueEmailValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEmail {

    String message() default EMAIL_IS_TAKEN_MSG;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
