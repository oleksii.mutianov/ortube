package com.ortube.validation.annotation

import com.ortube.constants.INVALID_IMAGE_URL
import com.ortube.validation.validator.UsersImageUrlValidator
import javax.validation.Constraint
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [UsersImageUrlValidator::class])
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class UsersImageUrl(

    val message: String = INVALID_IMAGE_URL,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []

)