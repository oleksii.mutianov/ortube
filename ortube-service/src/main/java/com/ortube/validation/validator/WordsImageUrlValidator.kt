package com.ortube.validation.validator

import com.ortube.validation.annotation.WordsImageUrl
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class WordsImageUrlValidator : ConstraintValidator<WordsImageUrl, String> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?) =
            if (value != null) {
                Regex("http://res\\.cloudinary\\.com/ortube/image/upload/.*/words/.*\\.(jpg|png)").matches(value)
            } else true

}