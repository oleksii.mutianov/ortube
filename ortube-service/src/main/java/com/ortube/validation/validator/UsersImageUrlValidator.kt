package com.ortube.validation.validator

import com.ortube.validation.annotation.UsersImageUrl
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class UsersImageUrlValidator : ConstraintValidator<UsersImageUrl, String> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?) =
            if (value != null) {
                Regex("http://res\\.cloudinary\\.com/ortube/image/upload/.*/users/.*\\.(jpg|png)").matches(value)
            } else true

}