package com.ortube.validation.validator

import com.ortube.validation.annotation.PlaylistsImageUrl
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class PlaylistsImageUrlValidator : ConstraintValidator<PlaylistsImageUrl, String> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?) =
            if (value != null) {
                Regex("http://res\\.cloudinary\\.com/ortube/image/upload/.*/playlists/.*\\.(jpg|png)").matches(value)
            } else true

}