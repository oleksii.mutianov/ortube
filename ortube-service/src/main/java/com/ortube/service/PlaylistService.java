package com.ortube.service;

import com.ortube.dto.PlaylistBriefDto;
import com.ortube.dto.PlaylistRequestDto;
import com.ortube.persistence.domain.Playlist;

import java.util.List;

public interface PlaylistService {

    List<PlaylistBriefDto> findAll();

    Playlist findById(String id);

    Playlist create(PlaylistRequestDto playlistRequestDto);

    Playlist updatePlaylist(String id, PlaylistRequestDto playlistRequestDto);

    void deletePlaylist(String id);

}
