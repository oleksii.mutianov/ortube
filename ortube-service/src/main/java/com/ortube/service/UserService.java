package com.ortube.service;

import com.ortube.persistence.domain.User;

public interface UserService {

    User findByEmailOrUsername(String emailOrUserName);

    User save(User user);

    User deleteActivationCode(String activationCode);

}
