package com.ortube.service

import com.ortube.constants.enums.UploadFolderName
import com.ortube.dto.ImageUploadResponse
import org.springframework.web.multipart.MultipartFile

interface ImageUploadService {
    fun upload(folder: UploadFolderName, file: MultipartFile?): ImageUploadResponse
}