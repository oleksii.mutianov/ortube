package com.ortube.service;

import com.ortube.dto.WordRequestDto;
import com.ortube.persistence.domain.AbstractPlaylist;

import java.util.List;

public interface WordService<T extends AbstractPlaylist> {

    T addWordToPlaylist(String playlistId, WordRequestDto wordDto);

    T update(String playlistId, String wordId, WordRequestDto wordDto);

    void deleteByIdAndPlaylistId(String wordId, String playlistId);

    void deleteAllByIdAndPlaylistId(List<String> wordIds, String playlistId);

}
