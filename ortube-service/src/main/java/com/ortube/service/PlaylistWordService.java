package com.ortube.service;

import com.ortube.persistence.domain.Playlist;

public interface PlaylistWordService extends WordService<Playlist> {

}
