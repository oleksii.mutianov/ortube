package com.ortube.service;

import com.ortube.dto.UserPlaylistBriefDto;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;

import java.util.List;

public interface UserPlaylistService {

    List<UserPlaylistBriefDto> findAllSortedByLastModified();

    List<UserPlaylistBriefDto> findAllPublicSortedByLastModified(String userId);

    UserPlaylist findById(String userPlaylistId);

    UserPlaylist save(UserPlaylistRequestDto userPlaylistRequestDto);

    UserPlaylist addPlaylist(String playlistId);

    UserPlaylist addPlaylistFromUser(String userIdCopyFrom, String userPlaylistId);

    UserPlaylist update(String userPlaylistId, UserPlaylistRequestDto userPlaylistRequestDto);

    void delete(String userPlaylistId);

}
