package com.ortube.service.impl;

import com.ortube.dto.UserPlaylistBriefDto;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.exceptions.PlaylistNotFoundException;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import com.ortube.persistence.repository.PlaylistRepository;
import com.ortube.persistence.repository.UserPlaylistRepository;
import com.ortube.security.model.UserContextHolder;
import com.ortube.service.UserPlaylistService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ortube.constants.ErrorMessagesKt.PLAYLIST_NOT_FOUND_MSG;
import static com.ortube.constants.ErrorMessagesKt.YOU_CANNOT_COPY_THIS_PLAYLIST_MSG;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_LAST_MODIFIED_DATE;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@RequiredArgsConstructor
public class UserPlaylistServiceImpl implements UserPlaylistService {

    private final UserPlaylistRepository userPlaylistRepository;
    private final PlaylistRepository playlistRepository;
    private final ConversionService conversionService;
    private final UserContextHolder userContextHolder;

    @Override
    public List<UserPlaylistBriefDto> findAllSortedByLastModified() {
        Sort sort = Sort.by(DESC, PLAYLIST_LAST_MODIFIED_DATE);
        return userPlaylistRepository.findAllByUserId(userContextHolder.getUserId(), sort)
                .stream()
                .map(userPlaylist -> conversionService.convert(userPlaylist, UserPlaylistBriefDto.class))
                .collect(toList());
    }

    @Override
    public List<UserPlaylistBriefDto> findAllPublicSortedByLastModified(String userId) {
        Sort sort = Sort.by(DESC, PLAYLIST_LAST_MODIFIED_DATE);
        return userPlaylistRepository.findAllByUserId(userId, sort)
                .stream()
                .map(userPlaylist -> conversionService.convert(userPlaylist, UserPlaylistBriefDto.class))
                .filter(userPlaylist -> userPlaylist.getPrivacy() == PlaylistPrivacy.PUBLIC)
                .collect(toList());
    }

    @Override
    public UserPlaylist findById(String userPlaylistId) {
        return userPlaylistRepository.findByIdAndUserId(userPlaylistId, userContextHolder.getUserId())
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG));
    }

    @Override
    public UserPlaylist save(UserPlaylistRequestDto userPlaylistRequestDto) {
        UserPlaylist userPlaylist = conversionService.convert(userPlaylistRequestDto, UserPlaylist.class);
        userPlaylist.setUserId(userContextHolder.getUserId());

        return userPlaylistRepository.save(userPlaylist);
    }

    @Override
    public UserPlaylist addPlaylist(String playlistId) {
        Playlist playlist = playlistRepository.findById(playlistId)
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG, playlistId));

        UserPlaylist userPlaylist = conversionService.convert(playlist, UserPlaylist.class);
        userPlaylist.setUserId(userContextHolder.getUserId());

        return userPlaylistRepository.save(userPlaylist);
    }

    @Override
    public UserPlaylist addPlaylistFromUser(String userIdCopyFrom, String userPlaylistId) {

        UserPlaylist userPlaylist = userPlaylistRepository.findByIdAndUserId(userPlaylistId, userIdCopyFrom)
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG));

        if (userPlaylist.getPrivacy() != PlaylistPrivacy.PUBLIC) {
            throw new AccessDeniedException(YOU_CANNOT_COPY_THIS_PLAYLIST_MSG);
        }

        UserPlaylist userPlaylistCopy = UserPlaylist.builder()
                .topic(userPlaylist.getTopic())
                .title(userPlaylist.getTitle())
                .words(userPlaylist.getWords())
                .userId(userContextHolder.getUserId())
                .build();

        return userPlaylistRepository.save(userPlaylistCopy);
    }

    @Override
    public UserPlaylist update(String userPlaylistId, UserPlaylistRequestDto userPlaylistRequestDto) {

        String userId = userContextHolder.getUserId();

        userPlaylistRepository.findByIdAndUserId(userPlaylistId, userId)
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG));

        UserPlaylist userPlaylist = conversionService.convert(userPlaylistRequestDto, UserPlaylist.class);
        return userPlaylistRepository.update(userPlaylistId, userPlaylist, userId);
    }

    @Override
    public void delete(String userPlaylistId) {
        userPlaylistRepository.delete(this.findById(userPlaylistId));
    }
}
