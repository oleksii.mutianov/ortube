package com.ortube.service.impl

import com.ortube.constants.TOO_FEW_ANSWERS_MSG
import com.ortube.constants.TOO_FEW_WORDS_MSG
import com.ortube.constants.enums.ImageMode
import com.ortube.constants.enums.QuizMode
import com.ortube.dto.QuizDto
import com.ortube.dto.QuizResultDto
import com.ortube.exceptions.PlaylistNotFoundException
import com.ortube.exceptions.QuizNotFoundException
import com.ortube.exceptions.TooFewWordsException
import com.ortube.extensions.MIN_ALLOWED_WORDS_COUNT
import com.ortube.extensions.getCorrectOptions
import com.ortube.extensions.getIncorrectOptions
import com.ortube.extensions.getOptionsToStartQuiz
import com.ortube.extensions.removeNonLetters
import com.ortube.persistence.domain.AnswerResult
import com.ortube.persistence.domain.Quiz
import com.ortube.persistence.domain.enums.AnswerStatus
import com.ortube.persistence.repository.QuizRepository
import com.ortube.persistence.repository.UserPlaylistRepository
import com.ortube.security.model.UserContextHolder
import com.ortube.service.QuizService
import org.springframework.stereotype.Service

@Service
class QuizServiceImpl(
    private val quizRepository: QuizRepository,
    private val userPlaylistRepository: UserPlaylistRepository,
    private val userContextHolder: UserContextHolder
) : QuizService {

    /**
     * Method that starting quiz. Takes id of playlist to start.
     * Returns QuizDto that contains generated id of Quiz document
     * and distinct shuffled list of words.
     * To start quiz playlist must contains at least 2 words.
     *
     * @param userPlaylistId id of playlist to start
     * @return generated id of Quiz document and distinct list of words
     * @throws TooFewWordsException if playlist has less than 2 words
     */
    override fun startQuiz(userPlaylistId: String, mode: QuizMode, image: ImageMode): QuizDto {
        val userId = userContextHolder.userId
        val userPlaylist = userPlaylistRepository.findByIdAndUserId(userPlaylistId, userId)
                .orElseThrow { PlaylistNotFoundException() }

        val words = userPlaylist.words.apply {
            if (size < MIN_ALLOWED_WORDS_COUNT) {
                throw TooFewWordsException(TOO_FEW_WORDS_MSG)
            }
        }

        val savedQuiz = quizRepository.save(Quiz(userId = userId, playlistId = userPlaylistId, words = words))

        val quizOptions = words.getOptionsToStartQuiz(mode, image)

        return QuizDto(savedQuiz.id, quizOptions)
    }

    /**
     * Returns answer options for the given word.
     * Contain at least 1 correct option and at least 2 incorrect.
     *
     * @param quizId    id of Quiz that user passing
     * @param wordTitle given word need to get options for
     * @return list of found options
     */
    override fun getOptions(quizId: String, wordTitle: String): List<String> {
        val words = getQuiz(quizId).words
        val correctOptions = words.getCorrectOptions(wordTitle)

        return words.getIncorrectOptions(wordTitle, correctOptions.size).plus(correctOptions).shuffled()
    }

    /**
     * Returns QuizResultDto that contain word that user answered,
     * AnswerStatus (RIGHT or WRONG) and list of correct options.
     * AnswerStatus will be RIGHT only if all right options are chosen.
     *
     * @param quizId      id of Quiz that user passing
     * @param wordTitle   word that user answered
     * @param userAnswers list of user answers
     * @return word that user answered, AnswerStatus (RIGHT or WRONG) and list of correct options.
     * @throws TooFewWordsException if userAnswers is empty
     */
    override fun giveAnAnswer(quizId: String, wordTitle: String, userAnswers: List<String>?): AnswerResult {
        if (userAnswers.isNullOrEmpty()) {
            throw TooFewWordsException(TOO_FEW_ANSWERS_MSG)
        }

        val quiz = getQuiz(quizId)
        val correctOptions = quiz.words.getCorrectOptions(wordTitle)
        val answerStatus = getAnswerStatus(userAnswers, correctOptions)

        val answerResult = AnswerResult(wordTitle, answerStatus, userAnswers, correctOptions)
        quiz.addResult(answerResult)

        quizRepository.save(quiz)

        return answerResult
    }

    override fun finishQuiz(quizId: String): QuizResultDto {
        val quiz = getQuiz(quizId).apply {
            finished = true
            words = emptyList()
            if (answers.size > 0) {
                result = (answers.filter { it.answerStatus == AnswerStatus.RIGHT }.count() * 100) / answers.size
            }
        }
        quizRepository.save(quiz)

        return QuizResultDto(quiz.result, quiz.answers)
    }

    private fun getQuiz(quizId: String) =
            quizRepository.findByUserIdAndIdAndFinishedIsFalse(userContextHolder.userId, quizId)
                    .orElseThrow { QuizNotFoundException() }

    private fun getAnswerStatus(userAnswers: List<String>, correctOptions: List<String>): AnswerStatus {
        val transformedUserAnswers = userAnswers.map { it.removeNonLetters() }
        val transformedCorrectOptions = correctOptions.map { it.removeNonLetters() }

        return if (!transformedUserAnswers.containsAll(transformedCorrectOptions) || !transformedCorrectOptions.containsAll(transformedUserAnswers)) {
            AnswerStatus.WRONG
        } else {
            AnswerStatus.RIGHT
        }
    }

}