package com.ortube.service.impl;

import com.ortube.dto.PlaylistBriefDto;
import com.ortube.dto.PlaylistRequestDto;
import com.ortube.exceptions.PlaylistNotFoundException;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.repository.PlaylistRepository;
import com.ortube.service.PlaylistService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ortube.constants.ErrorMessagesKt.PLAYLIST_NOT_FOUND_MSG;
import static com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_LAST_MODIFIED_DATE;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@RequiredArgsConstructor
public class PlaylistServiceImpl implements PlaylistService {

    private final PlaylistRepository playlistRepository;
    private final ConversionService conversionService;

    @Override
    public List<PlaylistBriefDto> findAll() {
        Sort sort = Sort.by(DESC, PLAYLIST_LAST_MODIFIED_DATE);
        return playlistRepository.findAll(sort)
                .stream()
                .map(playlist -> conversionService.convert(playlist, PlaylistBriefDto.class))
                .collect(toList());
    }

    @Override
    public Playlist findById(String id) {
        return playlistRepository.findById(id)
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG, id));
    }

    @Override
    public Playlist create(PlaylistRequestDto playlistRequestDto) {
        Playlist playlistToSave = conversionService.convert(playlistRequestDto, Playlist.class);
        return playlistRepository.save(playlistToSave);
    }

    @Override
    public Playlist updatePlaylist(String id, PlaylistRequestDto playlistRequestDto) {
        this.findById(id);
        Playlist playlistToUpdate = conversionService.convert(playlistRequestDto, Playlist.class);
        return playlistRepository.update(id, playlistToUpdate);
    }

    @Override
    public void deletePlaylist(String id) {
        playlistRepository.delete(this.findById(id));
    }

}
