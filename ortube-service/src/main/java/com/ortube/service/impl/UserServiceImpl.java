package com.ortube.service.impl;

import com.ortube.exceptions.InvalidActivationCodeException;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import com.ortube.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.ortube.constants.ErrorMessagesKt.INVALID_ACTIVATION_CODE_MSG;
import static com.ortube.constants.ErrorMessagesKt.USER_NOT_FOUND_MSG;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User findByEmailOrUsername(String emailOrUsername) {
        return userRepository.findByEmailOrUsername(emailOrUsername, emailOrUsername)
                .orElseThrow(() -> new UsernameNotFoundException(USER_NOT_FOUND_MSG));
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User deleteActivationCode(String activationCode) {
        User user = userRepository.findByActivationCode_Code(activationCode)
                .orElseThrow(() -> new InvalidActivationCodeException(INVALID_ACTIVATION_CODE_MSG));

        user.setActivationCode(null);

        return userRepository.save(user);
    }

}
