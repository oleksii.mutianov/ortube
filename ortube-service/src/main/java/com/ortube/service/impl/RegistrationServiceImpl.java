package com.ortube.service.impl;

import com.ortube.dto.MockUserCreateDto;
import com.ortube.dto.UserAccountDto;
import com.ortube.dto.UserCreateDto;
import com.ortube.mail.service.MailSendingService;
import com.ortube.persistence.domain.ActivationCode;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.domain.enums.UserRole;
import com.ortube.service.RegistrationService;
import com.ortube.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final BCryptPasswordEncoder encoder;
    private final UserService userService;
    private final ConversionService conversionService;
    private final MailSendingService mailSendingService;

    @Value("${app.activationCode.expired}")
    private long activationCodeExpired;

    @Override
    public UserAccountDto register(UserCreateDto userCreateDto) {

        User userToSave = conversionService.convert(userCreateDto, User.class);
        String activationCode = RandomStringUtils.randomAlphabetic(50);

        userToSave.setPassword(encoder.encode(userCreateDto.getPassword()));
        userToSave.setUserRole(UserRole.USER);
        userToSave.setActivationCode(new ActivationCode(activationCode, LocalDateTime.now().plusSeconds(activationCodeExpired)));

        mailSendingService.sendActivationEmail(userToSave.getEmail(), userToSave.getNickname(), activationCode);

        User savedUser = userService.save(userToSave);

        return conversionService.convert(savedUser, UserAccountDto.class);
    }

    @Override
    public String createMockUser(MockUserCreateDto mockUserCreateDto) {
        mockUserCreateDto.setPassword(encoder.encode(mockUserCreateDto.getPassword()));
        User user = conversionService.convert(mockUserCreateDto, User.class);
        User savedUser = userService.save(user);
        return savedUser.getId();
    }

    @Override
    public String activateAccount(String activationCode) {
        userService.deleteActivationCode(activationCode);
        return "Account successfully activated!"; // TODO return link to front-end
    }

}
