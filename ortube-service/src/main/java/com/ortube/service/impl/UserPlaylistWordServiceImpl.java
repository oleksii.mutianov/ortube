package com.ortube.service.impl;

import com.ortube.exceptions.PlaylistNotFoundException;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.repository.UserPlaylistRepository;
import com.ortube.persistence.repository.WordRepository;
import com.ortube.security.model.UserContextHolder;
import com.ortube.service.UserPlaylistWordService;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import static com.ortube.constants.ErrorMessagesKt.PLAYLIST_NOT_FOUND_MSG;

@Service
public class UserPlaylistWordServiceImpl extends AbstractWordServiceImpl<UserPlaylist> implements UserPlaylistWordService {

    private final UserPlaylistRepository userPlaylistRepository;
    private final UserContextHolder userContextHolder;

    public UserPlaylistWordServiceImpl(ConversionService conversionService,
                                       WordRepository<UserPlaylist> wordRepository,
                                       UserPlaylistRepository userPlaylistRepository,
                                       UserContextHolder userContextHolder) {
        super(wordRepository, conversionService, UserPlaylist.class);
        this.userPlaylistRepository = userPlaylistRepository;
        this.userContextHolder = userContextHolder;
    }

    @Override
    public UserPlaylist getPlaylistById(String userPlaylistId) {
        return userPlaylistRepository.findByIdAndUserId(userPlaylistId, userContextHolder.getUserId())
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG));
    }
}
