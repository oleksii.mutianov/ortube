package com.ortube.service.impl;

import com.ortube.exceptions.PlaylistNotFoundException;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.repository.PlaylistRepository;
import com.ortube.persistence.repository.WordRepository;
import com.ortube.service.PlaylistWordService;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import static com.ortube.constants.ErrorMessagesKt.PLAYLIST_NOT_FOUND_MSG;

@Service
public class PlaylistWordServiceImpl extends AbstractWordServiceImpl<Playlist> implements PlaylistWordService {

    private final PlaylistRepository playlistRepository;

    public PlaylistWordServiceImpl(ConversionService conversionService,
                                   WordRepository<Playlist> wordRepository,
                                   PlaylistRepository playlistRepository) {
        super(wordRepository, conversionService, Playlist.class);
        this.playlistRepository = playlistRepository;
    }

    @Override
    public Playlist getPlaylistById(String playlistId) {
        return playlistRepository.findById(playlistId)
                .orElseThrow(() -> new PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG, playlistId));
    }
}
