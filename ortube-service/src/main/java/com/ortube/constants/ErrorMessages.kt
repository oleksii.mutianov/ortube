package com.ortube.constants

const val PLAYLIST_NOT_FOUND_MSG = "Playlist not found!"
const val WORD_NOT_FOUND_MSG = "Word not found!"
const val USER_NOT_FOUND_MSG = "User not found!"
const val QUIZ_NOT_FOUND_MSG = "Quiz not found!"
const val CORRECT_OPTIONS_NOT_FOUND_MSG = "Right options not found"

const val CANNOT_CREATE_JWT_TOKEN_MSG = "Cannot create JWT Token without username"
const val INVALID_TOKEN_MSG = "JWT token is expired or invalid"
const val MUST_BE_ONLY_REFRESH_TOKEN_MSG = "Must be only refresh token"
const val INVALID_TOKEN_SCOPE_MSG = "Invalid token scope"

const val VALIDATION_FAILED_MSG = "Validation failed"
const val INVALID_USERNAME_OR_PASSWORD_MSG = "Invalid username or password"
const val AUTHENTICATION_FAILED_MSG = "Authentication failed"
const val AUTH_HEADER_CANNOT_BE_BLANK_MSG = "Authorization header cannot be blank!"
const val FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG = "Fingerprint header cannot be blank!"

const val YOU_CANNOT_COPY_THIS_PLAYLIST_MSG = "You cannot copy this playlist!"

const val EMAIL_IS_TAKEN_MSG = "email is taken"
const val USERNAME_IS_TAKEN_MSG = "username is taken"
const val INVALID_ACTIVATION_CODE_MSG = "Oops... Seems like your activation code is expired or invalid."

const val TOO_FEW_WORDS_MSG = "You cannot start quiz with less than 2 words"
const val TOO_FEW_ANSWERS_MSG = "Must be at least 1 answer"

const val UNSUPPORTED_IMAGE_FORMAT_MSG = "Unsupported image format"
const val MAXIMUM_UPLOAD_SIZE_MSG = "Maximum upload size exceeded"
const val FILE_IS_REQUIRED_MSG = "File us required"

const val VALUE_IS_INVALID = "Value '%s' is invalid"

const val INVALID_IMAGE_URL = "invalid image url"
const val MUST_NOT_BE_EMPTY = "must not be empty"
const val MUST_NOT_BE_NULL = "must not be null"

const val UNKNOWN_MODE_MSG = "Unknown mode"