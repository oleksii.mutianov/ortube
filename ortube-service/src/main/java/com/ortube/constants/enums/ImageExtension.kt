package com.ortube.constants.enums

enum class ImageExtension {
    JPG,
    JPEG,
    PNG,
}