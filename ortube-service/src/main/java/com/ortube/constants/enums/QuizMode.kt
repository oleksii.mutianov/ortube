package com.ortube.constants.enums

enum class QuizMode(val modeName: String) {
    LANG1_LANG2("lang1-lang2"),
    LANG2_LANG1("lang2-lang1"),
    MIXED("mixed")
}