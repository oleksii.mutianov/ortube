package com.ortube.constants.enums

enum class ImageMode(val modeName: String) {

    NO_IMAGE("no-image"),
    WITH_IMAGE("with-image"),

}