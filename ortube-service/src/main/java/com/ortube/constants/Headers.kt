package com.ortube.constants

const val AUTHENTICATION_HEADER = "Authorization"
const val BROWSER_FINGERPRINT_HEADER = "Browser-Fingerprint"
const val HEADER_PREFIX = "Bearer "