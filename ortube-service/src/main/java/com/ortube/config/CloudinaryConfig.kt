package com.ortube.config

import com.cloudinary.Cloudinary
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CloudinaryConfig(
    private val cloudinaryProperties: CloudinaryProperties
) {

    @Bean
    fun cloudinary() = Cloudinary(mapOf(
            Pair("cloud_name", cloudinaryProperties.cloudname),
            Pair("api_key", cloudinaryProperties.apikey),
            Pair("api_secret", cloudinaryProperties.apisecret)
    ))

    @Configuration
    @ConfigurationProperties("cloudinary")
    class CloudinaryProperties(
        var cloudname: String = "",
        var apikey: String = "",
        var apisecret: String = ""
    )
}

