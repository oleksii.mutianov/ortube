package com.ortube.security.jwt.validator;

import java.util.Map;

public interface TokenValidator {
    boolean validateToken(Map<String, String> headers);
}
