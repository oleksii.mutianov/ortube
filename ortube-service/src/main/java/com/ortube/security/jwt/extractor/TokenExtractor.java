package com.ortube.security.jwt.extractor;

import com.ortube.security.model.TokenScopes;

import java.time.LocalDateTime;

public interface TokenExtractor {

    String extractToken(String payload);

    String extractSubject(String token);

    TokenScopes extractScope(String token);

    LocalDateTime extractExpiredAt(String token);

}