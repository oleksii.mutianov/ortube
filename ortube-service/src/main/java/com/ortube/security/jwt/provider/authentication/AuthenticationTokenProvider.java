package com.ortube.security.jwt.provider.authentication;

import org.springframework.security.core.Authentication;

public interface AuthenticationTokenProvider {
    Authentication getAuthentication(String token);
}
