package com.ortube.security.jwt.provider.token;

public interface JwtTokenProvider {

    String createAccessToken(String subject);

    String createRefreshToken(String subject);

}
