package com.ortube.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.experimental.UtilityClass;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@UtilityClass
public class JwtUtils {

    public String build(Claims claims, LocalDateTime currentTime, long expTime, String tokenSigningKey) {
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(Timestamp.valueOf(currentTime))
                .setExpiration(Timestamp.valueOf(currentTime.plusSeconds(expTime)))
                .signWith(Keys.hmacShaKeyFor(tokenSigningKey.getBytes()))
                .compact();
    }

    public Claims parse(String tokenSigningKey, String token) {
        return Jwts.parser()
                .setSigningKey(Keys.hmacShaKeyFor(tokenSigningKey.getBytes()))
                .parseClaimsJws(token)
                .getBody();
    }
}
