package com.ortube.security.jwt.validator;

import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.model.TokenScopes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;

@Component
@TargetToken(TokenScopes.ACCESS_TOKEN)
public class AccessTokenValidator extends AbstractTokenValidator {

    @Autowired
    public AccessTokenValidator(TokenExtractor tokenExtractor) {
        super(tokenExtractor);
    }

    @Override
    public boolean validateToken(Map<String, String> headers) {
        return isNotExpired(headers.get(AUTHENTICATION_HEADER));
    }

}
