package com.ortube.security.jwt.provider.token;

import com.ortube.exceptions.MissingRequiredArgumentException;
import com.ortube.security.jwt.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.ortube.constants.ErrorMessagesKt.CANNOT_CREATE_JWT_TOKEN_MSG;
import static com.ortube.security.model.TokenScopes.ACCESS_TOKEN;
import static com.ortube.security.model.TokenScopes.REFRESH_TOKEN;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
@RequiredArgsConstructor
public class JwtTokenProviderImpl implements JwtTokenProvider {

    @Value("${jwt.token.secret}")
    private String tokenSigningKey;

    @Value("${jwt.token.expired}")
    private long accessTokenExpirationTime;

    @Value("${jwt.refreshToken.expired}")
    private long refreshTokenExpirationTime;

    @Override
    public String createAccessToken(String subject) {
        return createToken(subject, ACCESS_TOKEN.getValue(), accessTokenExpirationTime);
    }

    @Override
    public String createRefreshToken(String subject) {
        return createToken(subject, REFRESH_TOKEN.getValue(), refreshTokenExpirationTime);
    }

    private String createToken(String subject, String scope, long expTime) {
        if (isBlank(subject)) {
            throw new MissingRequiredArgumentException(CANNOT_CREATE_JWT_TOKEN_MSG);
        }

        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scope", scope);

        LocalDateTime currentTime = LocalDateTime.now();

        return JwtUtils.build(claims, currentTime, expTime, tokenSigningKey);
    }

}
