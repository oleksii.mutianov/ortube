package com.ortube.security.facade;

import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.dto.LoginRequestDto;
import com.ortube.dto.UserAccountDto;
import com.ortube.exceptions.JwtAuthenticationException;
import com.ortube.persistence.domain.User;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.model.TokenScopes;
import com.ortube.security.model.UserContextHolder;
import com.ortube.service.AuthService;
import com.ortube.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.ortube.constants.ErrorMessagesKt.AUTH_HEADER_CANNOT_BE_BLANK_MSG;
import static com.ortube.constants.ErrorMessagesKt.FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG;
import static com.ortube.constants.ErrorMessagesKt.INVALID_USERNAME_OR_PASSWORD_MSG;
import static com.ortube.constants.ErrorMessagesKt.MUST_BE_ONLY_REFRESH_TOKEN_MSG;

@Service
@RequiredArgsConstructor
public class AuthFacadeImpl implements AuthFacade {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final AuthService authService;
    private final TokenExtractor tokenExtractor;
    private final ConversionService conversionService;
    private final UserContextHolder userContextHolder;

    @Override
    public AuthenticationResponseDto authenticate(LoginRequestDto loginRequestDto) {
        String username = loginRequestDto.getUsername();
        String password = loginRequestDto.getPassword();
        String fingerprint = userContextHolder.getFingerprint()
                .orElseThrow(() -> new AuthenticationServiceException(FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG));
        try {
            User user = userService.findByEmailOrUsername(username);
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return authService.createTokens(user, fingerprint);
        } catch (BadCredentialsException | UsernameNotFoundException e) {
            throw new BadCredentialsException(INVALID_USERNAME_OR_PASSWORD_MSG, e);
        }
    }

    @Override
    public AuthenticationResponseDto refreshAccessToken() {

        String refreshToken = userContextHolder.getToken()
                .orElseThrow(() -> new AuthenticationServiceException(AUTH_HEADER_CANNOT_BE_BLANK_MSG));

        TokenScopes scope = tokenExtractor.extractScope(refreshToken);
        if (scope != TokenScopes.REFRESH_TOKEN) {
            throw new JwtAuthenticationException(MUST_BE_ONLY_REFRESH_TOKEN_MSG);
        }
        String username = tokenExtractor.extractSubject(refreshToken);
        User user = userService.findByEmailOrUsername(username);

        String fingerprint = userContextHolder.getFingerprint()
                .orElseThrow(() -> new AuthenticationServiceException(FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG));

        return authService.refreshTokens(user, refreshToken, fingerprint);
    }

    @Override
    public UserAccountDto getMe() {
        return conversionService.convert(userContextHolder.getUser(), UserAccountDto.class);
    }

}
