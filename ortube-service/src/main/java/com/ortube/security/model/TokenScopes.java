package com.ortube.security.model;

import com.ortube.constants.ErrorMessagesKt;
import com.ortube.exceptions.JwtAuthenticationException;
import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public enum TokenScopes {

    ACCESS_TOKEN("access"),
    REFRESH_TOKEN("refresh");

    @Getter
    private String value;

    TokenScopes(String value) {
        this.value = value;
    }

    public static TokenScopes fromString(String s) {
        if (isNotEmpty(s)) {
            for (TokenScopes value : TokenScopes.values()) {
                if (s.trim().equalsIgnoreCase(value.getValue())) {
                    return value;
                }
            }
        }
        throw new JwtAuthenticationException(ErrorMessagesKt.INVALID_TOKEN_SCOPE_MSG);
    }

}
