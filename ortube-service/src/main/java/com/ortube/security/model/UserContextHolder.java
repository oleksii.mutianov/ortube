package com.ortube.security.model;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Optional;

@Component
@RequestScope
@Data
public class UserContextHolder {
    private String token;
    private String fingerprint;
    private JwtUser user;

    public String getUserId() {
        return user.getId();
    }

    public Optional<String> getToken() {
        return Optional.ofNullable(token);
    }

    public Optional<String> getFingerprint() {
        return Optional.ofNullable(fingerprint);
    }

}
