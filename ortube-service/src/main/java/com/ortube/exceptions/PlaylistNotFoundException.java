package com.ortube.exceptions;

import static com.ortube.constants.ErrorMessagesKt.PLAYLIST_NOT_FOUND_MSG;

public class PlaylistNotFoundException extends EntityNotFoundException {
    public PlaylistNotFoundException(String message, String id) {
        super(message, id);
    }

    public PlaylistNotFoundException() {
        super(PLAYLIST_NOT_FOUND_MSG);
    }

    public PlaylistNotFoundException(String message) {
        super(message);
    }
}
