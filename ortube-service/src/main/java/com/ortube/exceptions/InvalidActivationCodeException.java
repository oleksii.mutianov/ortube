package com.ortube.exceptions;

public class InvalidActivationCodeException extends EntityNotFoundException {
    public InvalidActivationCodeException(String message) {
        super(message);
    }
}
