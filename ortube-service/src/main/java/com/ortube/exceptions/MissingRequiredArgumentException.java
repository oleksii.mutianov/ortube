package com.ortube.exceptions;

public class MissingRequiredArgumentException extends RuntimeException {
    public MissingRequiredArgumentException(String message) {
        super(message);
    }
}
