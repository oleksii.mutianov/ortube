package com.ortube.exceptions

class TooFewWordsException(message: String) : RuntimeException(message)