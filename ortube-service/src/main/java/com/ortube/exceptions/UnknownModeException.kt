package com.ortube.exceptions

class UnknownModeException(message: String) : RuntimeException(message)