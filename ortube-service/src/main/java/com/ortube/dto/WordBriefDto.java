package com.ortube.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WordBriefDto {

    private String lang1;
    private String lang2;

}
