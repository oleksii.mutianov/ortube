package com.ortube.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PlaylistBriefDto {

    private String id;
    private String title;
    private String topic;
    private String image;
    private Integer wordCount;
    private List<WordBriefDto> words;

}
