package com.ortube.dto;

import com.ortube.persistence.domain.enums.UserRole;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserAccountDto {

    private String id;
    private String email;
    private UserRole role;
    private String username;
    private String nickname;
    private String image;

}
