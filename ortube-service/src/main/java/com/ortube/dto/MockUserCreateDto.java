package com.ortube.dto;

import com.ortube.persistence.domain.enums.UserRole;
import com.ortube.validation.annotation.UniqueEmail;
import com.ortube.validation.annotation.UniqueUsername;
import com.ortube.validation.annotation.UsersImageUrl;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MockUserCreateDto {

    @UniqueEmail
    private String email;
    private String password;
    private UserRole role;
    @UniqueUsername
    private String userName;
    private String nickName;
    @UsersImageUrl
    private String image;

}
