package com.ortube.dto

import com.ortube.persistence.domain.enums.PlaylistPrivacy

data class UserPlaylistBriefDto(
    val id: String,
    val title: String,
    val topic: String?,
    val image: String?,
    val privacy: PlaylistPrivacy,
    val wordCount: Int,
    val words: List<WordBriefDto>
)