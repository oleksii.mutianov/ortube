package com.ortube.dto

import com.ortube.validation.annotation.WordsImageUrl
import javax.validation.constraints.NotEmpty

data class WordRequestDto(

    @get:NotEmpty
    val lang1: String?,

    @get:NotEmpty
    val lang2: String?,

    @get:WordsImageUrl
    var image: String? = null

)