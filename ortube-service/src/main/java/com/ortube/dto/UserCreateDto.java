package com.ortube.dto;

import com.ortube.validation.annotation.UniqueEmail;
import com.ortube.validation.annotation.UniqueUsername;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {

    @Email
    @NotEmpty
    @UniqueEmail
    private String email;

    @NotEmpty
    // TODO: regex for password
    private String password;

    @UniqueUsername
    private String username;

    @NotEmpty
    private String nickname;

}
