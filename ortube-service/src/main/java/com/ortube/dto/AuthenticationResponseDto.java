package com.ortube.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class AuthenticationResponseDto {

    private String role;
    private String accessToken;
    private LocalDateTime expiredAt;
    private String refreshToken;

}
