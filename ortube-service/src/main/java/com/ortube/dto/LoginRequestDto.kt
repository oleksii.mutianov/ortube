package com.ortube.dto

import javax.validation.constraints.NotEmpty

data class LoginRequestDto(

    @get:NotEmpty
    val username: String?,

    @get:NotEmpty
    val password: String?

)