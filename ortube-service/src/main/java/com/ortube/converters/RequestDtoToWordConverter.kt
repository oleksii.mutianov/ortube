package com.ortube.converters

import com.ortube.dto.WordRequestDto
import com.ortube.persistence.domain.Word
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class RequestDtoToWordConverter : Converter<WordRequestDto, Word> {
    override fun convert(source: WordRequestDto) = Word(
            lang1 = source.lang1!!,
            lang2 = source.lang2!!,
            image = source.image
    )
}