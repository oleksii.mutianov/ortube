package com.ortube.converters

import com.ortube.constants.UNKNOWN_MODE_MSG
import com.ortube.constants.enums.QuizMode
import com.ortube.exceptions.UnknownModeException
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class StringToQuizModeConverter : Converter<String, QuizMode> {

    override fun convert(source: String): QuizMode {
        QuizMode.values().forEach {
            if (it.modeName == source) {
                return it
            }
        }
        throw UnknownModeException("$UNKNOWN_MODE_MSG $source")
    }

}