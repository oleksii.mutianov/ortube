package com.ortube.converters;

import com.ortube.dto.UserAccountDto;
import com.ortube.persistence.domain.enums.UserRole;
import com.ortube.security.model.JwtUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JwtUserToUserAccountDtoConverter implements Converter<JwtUser, UserAccountDto> {
    @Override
    public UserAccountDto convert(JwtUser source) {
        return UserAccountDto.builder()
                .id(source.getId())
                .email(source.getEmail())
                .nickname(source.getNickname())
                .role(UserRole.valueOf(source.getAuthorities().iterator().next().getAuthority()))
                .username(source.getUsername())
                .image(source.getImage())
                .build();
    }
}
