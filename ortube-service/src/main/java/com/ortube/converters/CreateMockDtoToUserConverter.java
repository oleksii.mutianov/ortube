package com.ortube.converters;

import com.ortube.dto.MockUserCreateDto;
import com.ortube.persistence.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CreateMockDtoToUserConverter implements Converter<MockUserCreateDto, User> {
    @Override
    public User convert(MockUserCreateDto source) {
        return User.builder()
                .email(source.getEmail())
                .nickname(source.getNickName())
                .password(source.getPassword())
                .username(source.getUserName())
                .userRole(source.getRole())
                .image(source.getImage())
                .build();
    }
}
