package com.ortube.converters;

import com.ortube.persistence.domain.User;
import com.ortube.security.model.JwtUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;

@Component
public class UserToJwtUserDtoConverter implements Converter<User, JwtUser> {
    @Override
    public JwtUser convert(User source) {
        return JwtUser.builder()
                .id(source.getId())
                .email(source.getEmail())
                .username(source.getUsername())
                .password(source.getPassword())
                .nickname(source.getNickname())
                .enabled(isNull(source.getActivationCode())) // if activation code is present user is disabled
                .authorities(singletonList(new SimpleGrantedAuthority(source.getUserRole().name())))
                .image(source.getImage())
                .build();
    }
}
