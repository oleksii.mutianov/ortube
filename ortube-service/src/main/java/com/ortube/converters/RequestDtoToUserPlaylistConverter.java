package com.ortube.converters;

import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RequestDtoToUserPlaylistConverter implements Converter<UserPlaylistRequestDto, UserPlaylist> {
    @Override
    public UserPlaylist convert(UserPlaylistRequestDto userPlaylistRequestDto) {
        return UserPlaylist.builder()
                .title(userPlaylistRequestDto.getTitle())
                .topic(userPlaylistRequestDto.getTopic())
                .privacy(userPlaylistRequestDto.getPrivacy())
                .image(userPlaylistRequestDto.getImage())
                .build();
    }
}
