package com.ortube.converters;

import com.ortube.dto.UserAccountDto;
import com.ortube.persistence.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToAccountDtoConverter implements Converter<User, UserAccountDto> {
    @Override
    public UserAccountDto convert(User source) {
        return UserAccountDto.builder()
                .id(source.getId())
                .email(source.getEmail())
                .nickname(source.getNickname())
                .role(source.getUserRole())
                .username(source.getUsername())
                .image(source.getImage())
                .build();
    }
}
