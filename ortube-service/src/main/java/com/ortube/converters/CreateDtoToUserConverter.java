package com.ortube.converters;

import com.ortube.dto.UserCreateDto;
import com.ortube.persistence.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CreateDtoToUserConverter implements Converter<UserCreateDto, User> {
    @Override
    public User convert(UserCreateDto source) {
        return User.builder()
                .email(source.getEmail())
                .username(source.getUsername())
                .password(source.getPassword())
                .nickname(source.getNickname())
                .build();
    }
}
