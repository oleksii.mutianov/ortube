package com.ortube.converters;

import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.UserPlaylist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlaylistToUserPlaylistConverter implements Converter<Playlist, UserPlaylist> {
    @Override
    public UserPlaylist convert(Playlist playlist) {
        return UserPlaylist.builder()
                .title(playlist.getTitle())
                .topic(playlist.getTopic())
                .words(playlist.getWords())
                .image(playlist.getImage())
                .build();
    }
}
