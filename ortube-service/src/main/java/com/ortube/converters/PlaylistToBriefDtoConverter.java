package com.ortube.converters;

import com.ortube.dto.PlaylistBriefDto;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.Word;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class PlaylistToBriefDtoConverter implements Converter<Playlist, PlaylistBriefDto> {

    private final WordToBriefDtoConverter wordToBriefDtoConverter;

    @Override
    public PlaylistBriefDto convert(Playlist source) {
        List<Word> words = Optional.ofNullable(source.getWords()).orElse(emptyList());
        return PlaylistBriefDto.builder()
                .id(source.getId())
                .title(source.getTitle())
                .topic(source.getTopic())
                .wordCount(words.size())
                .words(words.stream().limit(3).map(wordToBriefDtoConverter::convert).collect(toList()))
                .image(source.getImage())
                .build();
    }

}
