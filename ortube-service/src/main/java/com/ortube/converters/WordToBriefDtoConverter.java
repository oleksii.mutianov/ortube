package com.ortube.converters;

import com.ortube.dto.WordBriefDto;
import com.ortube.persistence.domain.Word;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WordToBriefDtoConverter implements Converter<Word, WordBriefDto> {

    @Override
    public WordBriefDto convert(Word source) {
        return WordBriefDto.builder()
                .lang1(source.getLang1())
                .lang2(source.getLang2())
                .build();
    }

}
