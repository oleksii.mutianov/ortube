package com.ortube.converters;

import com.ortube.dto.PlaylistRequestDto;
import com.ortube.persistence.domain.Playlist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RequestDtoToPlaylistConverter implements Converter<PlaylistRequestDto, Playlist> {

    @Override
    public Playlist convert(PlaylistRequestDto source) {
        return Playlist.builder()
                .title(source.getTitle())
                .topic(source.getTopic())
                .image(source.getImage())
                .build();
    }

}
