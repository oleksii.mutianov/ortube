FROM adoptopenjdk:13-jre-openj9-bionic
WORKDIR /root/app
COPY ortube-web/target/'ortube-web-0.0.1-SNAPSHOT-exec.jar' /root/app/ortube.jar

EXPOSE 8080

CMD java -jar /root/app/ortube.jar

