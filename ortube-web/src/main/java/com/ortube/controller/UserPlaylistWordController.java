package com.ortube.controller;

import com.ortube.dto.WordRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.service.UserPlaylistWordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("secure/userPlaylist")
@Api(tags = "UserPlaylist API")
public class UserPlaylistWordController {

    private final UserPlaylistWordService userPlaylistWordService;

    @PostMapping("{userPlaylistId}/word")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Add word to playlist")
    public UserPlaylist addWord(@PathVariable String userPlaylistId,
                                @RequestBody @Valid WordRequestDto wordRequestDto) {
        return userPlaylistWordService.addWordToPlaylist(userPlaylistId, wordRequestDto);
    }

    @PutMapping("{userPlaylistId}/word/{wordId}")
    @ApiOperation("Update word in playlist")
    public UserPlaylist updateWord(@PathVariable String userPlaylistId,
                                   @PathVariable String wordId,
                                   @RequestBody @Valid WordRequestDto wordRequestDto) {

        return userPlaylistWordService.update(userPlaylistId, wordId, wordRequestDto);
    }

    @DeleteMapping("{userPlaylistId}/word/{wordId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete word from playlist by playlist id and word id")
    public void deleteWord(@PathVariable String userPlaylistId,
                           @PathVariable String wordId) {

        userPlaylistWordService.deleteByIdAndPlaylistId(wordId, userPlaylistId);
    }

    @DeleteMapping("{userPlaylistId}/word")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete several words from playlist")
    public void deleteWords(@PathVariable String userPlaylistId,
                            @ApiParam(value = "Array of word ids to delete", required = true)
                            @RequestBody List<String> wordIds) {

        userPlaylistWordService.deleteAllByIdAndPlaylistId(wordIds, userPlaylistId);
    }

}