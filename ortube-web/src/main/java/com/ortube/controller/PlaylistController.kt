package com.ortube.controller

import com.ortube.dto.PlaylistRequestDto
import com.ortube.security.annotation.Access
import com.ortube.service.PlaylistService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@Api(description = "REST API for playlists", tags = ["Playlist API"])
class PlaylistController(
    private val playlistService: PlaylistService
) {

    @GetMapping("playlist")
    @ApiOperation("Get all playlists")
    fun findAll() = playlistService.findAll()

    @GetMapping("playlist/{id}")
    @ApiOperation("Get playlist by id")
    fun findById(@PathVariable id: String) = playlistService.findById(id)

    @PostMapping("secure/playlist")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Create playlist")
    @Access.Admin
    fun createPlaylist(
        @RequestBody @Valid playlistRequestDto: PlaylistRequestDto
    ) = playlistService.create(playlistRequestDto)

    @PutMapping("secure/playlist/{id}")
    @ApiOperation("Update playlist by id")
    @Access.Admin
    fun updatePlaylist(
        @PathVariable id: String,
        @RequestBody @Valid playlistRequestDto: PlaylistRequestDto
    ) = playlistService.updatePlaylist(id, playlistRequestDto)

    @DeleteMapping("secure/playlist/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete playlist by id")
    @Access.Admin
    fun deletePlaylist(@PathVariable id: String) = playlistService.deletePlaylist(id)

}