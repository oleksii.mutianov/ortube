package com.ortube.controller

import com.ortube.constants.enums.ImageMode
import com.ortube.constants.enums.QuizMode
import com.ortube.service.QuizService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("secure/quiz")
@Api(description = "API for taking quizzes", tags = ["Quiz API"])
class QuizController(private val quizService: QuizService) {

    @PostMapping("start/{userPlaylistId}")
    @ApiOperation("Start taking quiz by user. Returns quizId and list of words for taking quiz.")
    fun startQuiz(
        @PathVariable userPlaylistId: String,

        @ApiParam(allowableValues = "lang1-lang2, lang2-lang1, mixed")
        @RequestParam(defaultValue = "lang1-lang2")
        mode: QuizMode,

        @ApiParam(allowableValues = "no-image, with-image")
        @RequestParam(defaultValue = "no-image")
        image: ImageMode
    ) = quizService.startQuiz(userPlaylistId, mode, image)

    @GetMapping("{quizId}/words/{wordTitle}")
    @ApiOperation("Get answer options for the given word. Contain at least 1 correct option.")
    fun getOptions(@PathVariable quizId: String, @PathVariable wordTitle: String) = quizService.getOptions(quizId, wordTitle)

    @PutMapping("{quizId}/words/{wordTitle}")
    @ApiOperation("Endpoint to send user's chosen options. Returns answer status (WRONG/RIGHT) and list of right options.")
    fun giveAnAnswer(
        @PathVariable quizId: String,
        @PathVariable wordTitle: String,
        @RequestBody userAnswers: List<String>?
    ) = quizService.giveAnAnswer(quizId, wordTitle, userAnswers)

    @PutMapping("{quizId}")
    @ApiOperation("Endpoint to finish taking quiz. After quiz finished it's impossible to give an answer for the quiz.")
    fun finishQuiz(@PathVariable quizId: String) = quizService.finishQuiz(quizId)

}