package com.ortube.controller;

import com.ortube.dto.UserPlaylistBriefDto;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.service.UserPlaylistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("secure/userPlaylist")
@Api(description = "REST API for user playlists", tags = "UserPlaylist API")
public class UserPlaylistController {

    private final UserPlaylistService userPlaylistService;

    @GetMapping
    @ApiOperation("Get all user's playlists")
    public List<UserPlaylistBriefDto> findAll() {
        return userPlaylistService.findAllSortedByLastModified();
    }

    @GetMapping("{userPlaylistId}")
    @ApiOperation("Get user's playlist by id")
    public UserPlaylist findById(@PathVariable String userPlaylistId) {
        return userPlaylistService.findById(userPlaylistId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Create playlist")
    public UserPlaylist createPlaylist(@RequestBody @Valid UserPlaylistRequestDto userPlaylistRequestDto) {
        return userPlaylistService.save(userPlaylistRequestDto);
    }

    @PutMapping("{userPlaylistId}")
    @ApiOperation("Update user's playlist by id")
    public UserPlaylist updatePlaylist(@RequestBody UserPlaylistRequestDto updates,
                                       @PathVariable String userPlaylistId) {
        return userPlaylistService.update(userPlaylistId, updates);
    }

    @DeleteMapping("{userPlaylistId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete user's playlist by id")
    public void deletePlaylist(@PathVariable String userPlaylistId) {
        userPlaylistService.delete(userPlaylistId);
    }

    @PostMapping("add/{playlistId}")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Add playlist from global playlist list")
    public UserPlaylist addPlaylist(@ApiParam(value = "Id of playlist from global playlist list", required = true)
                                    @PathVariable String playlistId) {
        return userPlaylistService.addPlaylist(playlistId);
    }

    @PostMapping("user/{userId}/add/{userPlaylistId}")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Add other user's playlist")
    public UserPlaylist copyPlaylistFromUser(@PathVariable String userId,
                                             @ApiParam(value = "Other user's playlist id", required = true)
                                             @PathVariable String userPlaylistId) {
        return userPlaylistService.addPlaylistFromUser(userId, userPlaylistId);
    }

    @GetMapping("user/{userId}")
    @ApiOperation("Get all user's public playlists by user id")
    public List<UserPlaylistBriefDto> findAllByUserId(@PathVariable String userId) {
        return userPlaylistService.findAllPublicSortedByLastModified(userId);
    }

}
