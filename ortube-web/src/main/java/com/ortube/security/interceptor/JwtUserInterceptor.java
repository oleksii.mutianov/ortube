package com.ortube.security.interceptor;

import com.ortube.constants.HeadersKt;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.model.JwtUser;
import com.ortube.security.model.UserContextHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@RequiredArgsConstructor
public class JwtUserInterceptor extends HandlerInterceptorAdapter {

    private final TokenExtractor tokenExtractor;
    private final UserContextHolder contextHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof JwtUser) {
            contextHolder.setUser((JwtUser) authentication.getPrincipal());
        }

        String authHeader = request.getHeader(HeadersKt.AUTHENTICATION_HEADER);
        contextHolder.setToken(tokenExtractor.extractToken(authHeader));

        String fingerprint = request.getHeader(HeadersKt.BROWSER_FINGERPRINT_HEADER);
        contextHolder.setFingerprint(fingerprint);

        return true;
    }
}
