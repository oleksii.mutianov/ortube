package com.ortube.security.filter;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomCorsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Expose-Headers", "x-token");
        response.setHeader("Access-Control-Allow-Headers",
                "Authorization, x-token, Content-Type, Accept, Origin, Access-Control-Expose-Headers, Browser-Fingerprint");
        response.setHeader("Access-Control-Max-Age", "3600");

        if (!Objects.equals(request.getMethod(), "OPTIONS")) {
            filterChain.doFilter(request, response);
        }
    }

}