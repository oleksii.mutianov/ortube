package com.ortube.exceptions;

import lombok.Getter;
import org.springframework.data.util.StreamUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Map;

import static com.ortube.constants.ErrorMessagesKt.VALIDATION_FAILED_MSG;

@Getter
public class ValidationErrorDetails extends ErrorDetails {

    private final Map<String, List<String>> validationErrors;

    public ValidationErrorDetails(List<FieldError> fieldErrors) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, VALIDATION_FAILED_MSG);
        this.validationErrors = fieldErrors
                .stream()
                .collect(StreamUtils.toMultiMap(FieldError::getField, FieldError::getDefaultMessage));
    }
}
