package com.ortube.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class ErrorDetails {

    private LocalDateTime timestamp;
    private Integer status;
    private String message;
    private String error;

    public ErrorDetails(HttpStatus httpStatus, String message) {
        this(LocalDateTime.now(), httpStatus.value(), message, httpStatus.getReasonPhrase());
    }

    @JsonIgnore
    public ResponseEntity<ErrorDetails> getResponseEntity() {
        return ResponseEntity.status(status).body(this);
    }

}

