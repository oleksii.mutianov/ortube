package com.ortube.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import static com.ortube.constants.ErrorMessagesKt.MAXIMUM_UPLOAD_SIZE_MSG;
import static com.ortube.constants.ErrorMessagesKt.VALUE_IS_INVALID;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorDetails> entityNotFoundHandler(EntityNotFoundException ex) {
        log.warn(ex.getDetails(), ex);
        return new ErrorDetails(NOT_FOUND, ex.getMessage()).getResponseEntity();
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ErrorDetails> authenticationExceptionHandler(AuthenticationException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(UNAUTHORIZED, ex.getMessage()).getResponseEntity();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDetails> methodArgumentNotValidHandler(MethodArgumentNotValidException ex) {
        log.warn(ex.getMessage(), ex);
        return new ValidationErrorDetails(ex.getBindingResult().getFieldErrors()).getResponseEntity();
    }

    @ExceptionHandler({MissingRequiredArgumentException.class, TooFewWordsException.class, InvalidImageFormatException.class})
    public ResponseEntity<ErrorDetails> missingRequiredArgumentHandler(RuntimeException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(BAD_REQUEST, ex.getMessage()).getResponseEntity();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorDetails> methodArgumentTypeMismatchHandler(MethodArgumentTypeMismatchException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(BAD_REQUEST, String.format(VALUE_IS_INVALID, ex.getValue())).getResponseEntity();
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<ErrorDetails> maxUploadSizeExceededHandler(MaxUploadSizeExceededException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(PAYLOAD_TOO_LARGE, MAXIMUM_UPLOAD_SIZE_MSG).getResponseEntity();
    }

    @ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<ErrorDetails> conversionFailedHandler(ConversionFailedException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(BAD_REQUEST, ex.getCause().getMessage()).getResponseEntity();
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorDetails> accessDeniedHandler(AccessDeniedException ex) {
        log.warn(ex.getMessage(), ex);
        return new ErrorDetails(FORBIDDEN, ex.getMessage()).getResponseEntity();
    }

    @ExceptionHandler(MailServiceException.class)
    public ResponseEntity<ErrorDetails> mailServiceExceptionHandler(MailServiceException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorDetails(INTERNAL_SERVER_ERROR, ex.getMessage()).getResponseEntity();
    }

}
