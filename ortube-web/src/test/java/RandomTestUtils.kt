import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic

fun randomWordsImageUrl() = "http://res.cloudinary.com/ortube/image/upload/v1593357278/words/${randomAlphabetic(10)}.jpg"
fun randomPlaylistsImageUrl() = "http://res.cloudinary.com/ortube/image/upload/v1593357278/playlists/${randomAlphabetic(10)}.jpg"