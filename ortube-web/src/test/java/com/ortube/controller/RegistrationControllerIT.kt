package com.ortube.controller

import com.ortube.config.AbstractSecurityIntegrationTest
import com.ortube.constants.BROWSER_FINGERPRINT_HEADER
import com.ortube.constants.EMAIL_IS_TAKEN_MSG
import com.ortube.constants.LOGIN_ENDPOINT
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.REGISTER_ENDPOINT
import com.ortube.constants.USERNAME_IS_TAKEN_MSG
import com.ortube.constants.VALIDATION_ERRORS_PATH
import com.ortube.constants.VALIDATION_FAILED_MSG
import com.ortube.dto.LoginRequestDto
import com.ortube.dto.UserCreateDto
import com.ortube.persistence.domain.ActivationCode
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.enums.UserRole
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.LocalDateTime

class RegistrationControllerIT : AbstractSecurityIntegrationTest() {

    companion object {
        const val USER_IS_DISABLED = "User is disabled"
    }

    @Test
    fun `Should register new user`() {
        // GIVEN
        val userCreateDto = userDataGenerator.userCreateDto()

        // WHEN
        val requestBuilder = post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCreateDto))

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.id").isNotEmpty)
                .andExpect(jsonPath("$.email").value(userCreateDto.email))
                .andExpect(jsonPath("$.role").value(UserRole.USER.name))
                .andExpect(jsonPath("$.username").value(userCreateDto.username))
                .andExpect(jsonPath("$.nickname").value(userCreateDto.nickname))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 even if username is empty`() {
        // GIVEN
        val userCreateDto = userDataGenerator.userCreateDto().apply { username = null }

        // WHEN
        val requestBuilder = post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCreateDto))

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.id").isNotEmpty)
                .andExpect(jsonPath("$.email").value(userCreateDto.email))
                .andExpect(jsonPath("$.role").value(UserRole.USER.name))
                .andExpect(jsonPath("$.username").doesNotExist()) // TODO: must be not empty but randomly generated
                .andExpect(jsonPath("$.nickname").value(userCreateDto.nickname))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 422 when user try to register with duplicated email`() {
        // GIVEN
        val userCreateDto = userDataGenerator.userCreateDto()

        mockMvc.perform(post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCreateDto))).andExpect(status().isOk)

        val userWithDuplicatedEmail = userDataGenerator.userCreateDto().apply { email = userCreateDto.email }

        // WHEN
        val requestBuilder = post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userWithDuplicatedEmail))

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALIDATION_FAILED_MSG))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.email[0]").value(EMAIL_IS_TAKEN_MSG))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 422 when user try to register with duplicated username`() {
        // GIVEN
        val userCreateDto: UserCreateDto = userDataGenerator.userCreateDto()

        mockMvc.perform(post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCreateDto))).andExpect(status().isOk)

        val userWithDuplicatedUsername = userDataGenerator.userCreateDto().apply { username = userCreateDto.username }

        // WHEN
        val requestBuilder = post(REGISTER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userWithDuplicatedUsername))

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALIDATION_FAILED_MSG))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.username[0]").value(USERNAME_IS_TAKEN_MSG))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 401 if not activated user trying to get access`() {
        // GIVEN
        val activationCode = randomAlphabetic(10)

        val user = mongoTemplate
                .findById(testUser.id, User::class.java)!!
                .apply { this.activationCode = ActivationCode(activationCode, LocalDateTime.now().plusSeconds(10)) }

        mongoTemplate.save(user)

        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(USER_IS_DISABLED))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

}