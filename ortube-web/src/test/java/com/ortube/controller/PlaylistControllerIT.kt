package com.ortube.controller

import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.INVALID_IMAGE_URL
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.MUST_NOT_BE_EMPTY
import com.ortube.constants.PLAYLIST_ENDPOINT
import com.ortube.constants.PLAYLIST_NOT_FOUND_MSG
import com.ortube.constants.SECURE_PLAYLIST_ENDPOINT
import com.ortube.constants.VALIDATION_ERRORS_PATH
import com.ortube.constants.VALIDATION_FAILED_MSG
import com.ortube.dto.PlaylistRequestDto
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.IMAGE
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.PLAYLIST_ID
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TITLE
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.TOPIC
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.WORDS
import com.ortube.persistence.domain.Playlist
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class PlaylistControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return 200 and empty array when getting all playlists and there is no playlists`() {

        // WHEN
        val requestBuilder = get(PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$", hasSize<Any>(0)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 and array with size of all playlists when getting all playlists`() {

        // GIVEN
        val playlists = listOf(playlistDataGenerator.playlist(),
                playlistDataGenerator.playlist(), playlistDataGenerator.playlist())

        // WHEN
        val requestBuilder = get(PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$", hasSize<Any>(playlists.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 response and playlist when getting playlist by id`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        val requestBuilder = get("$PLAYLIST_ENDPOINT/${playlist.id}")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").value(playlist.id))
                .andExpect(jsonPath("$.$TITLE").value(playlist.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlist.topic))
                .andExpect(jsonPath("$.$WORDS", hasSize<Any>(playlist.words.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 when playlist by id not found`() {

        // GIVEN
        val wrongId = "1"

        // WHEN
        val requestBuilder = get("$PLAYLIST_ENDPOINT/$wrongId")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 201 and playlist when creating playlist`() {

        // GIVEN
        val playlistRequestDto = playlistDataGenerator.playlistRequestDto()

        // WHEN
        val requestBuilder = post(SECURE_PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").isNotEmpty)
                .andExpect(jsonPath("$.$TITLE").value(playlistRequestDto.title!!))
                .andExpect(jsonPath("$.$TOPIC").value(playlistRequestDto.topic!!))
                .andExpect(jsonPath("$.$WORDS").doesNotExist())
                .andExpect(status().isCreated)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 422 if payload is invalid`() {
        // GIVEN
        val playlistRequestDto = PlaylistRequestDto(title = null, image = "invalid")

        // WHEN
        val requestBuilder = post(SECURE_PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALIDATION_FAILED_MSG))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$TITLE[0]").value(MUST_NOT_BE_EMPTY))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$IMAGE[0]").value(INVALID_IMAGE_URL))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 and update playlist when updating playlist`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val playlistRequestDto = playlistDataGenerator.playlistRequestDto()

        // WHEN
        val requestBuilder = put("$SECURE_PLAYLIST_ENDPOINT/${playlist.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").value(playlist.id))
                .andExpect(jsonPath("$.$TITLE").value(playlistRequestDto.title!!))
                .andExpect(jsonPath("$.$TOPIC").value(playlistRequestDto.topic!!))
                .andExpect(jsonPath("$.$WORDS", hasSize<Any>(playlist.words.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 if playlist not found when updating playlist`() {

        // GIVEN
        val playlistRequestDto = playlistDataGenerator.playlistRequestDto()

        // WHEN
        val requestBuilder = put("${SECURE_PLAYLIST_ENDPOINT}/wrongId")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 and delete playlist when deleting playlist`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        val requestBuilder = delete("$SECURE_PLAYLIST_ENDPOINT/${playlist.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNoContent)

        assertNull(mongoTemplate.findById(playlist.id, Playlist::class.java))
    }

    @Test
    fun `Should return 404 if playlist not found when deleting playlist`() {

        // GIVEN
        val wrongId = "1"

        // WHEN
        val requestBuilder = delete("$SECURE_PLAYLIST_ENDPOINT/$wrongId")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }
}