package com.ortube.controller

import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.ANSWERS_PATH
import com.ortube.constants.ANSWER_STATUS_PATH
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.CORRECT_OPTIONS_PATH
import com.ortube.constants.GET_QUIZ_OPTIONS_ENDPOINT
import com.ortube.constants.GIVE_QUIZ_ANSWER_ENDPOINT
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.QUIZ_ENDPOINT
import com.ortube.constants.QUIZ_ID_PATH
import com.ortube.constants.QUIZ_NOT_FOUND_MSG
import com.ortube.constants.RESULTS_PATH
import com.ortube.constants.START_QUIZ_ENDPOINT
import com.ortube.constants.TOO_FEW_ANSWERS_MSG
import com.ortube.constants.TOO_FEW_WORDS_MSG
import com.ortube.constants.USER_ANSWERS_PATH
import com.ortube.constants.VALUE_IS_INVALID
import com.ortube.constants.WORDS_PATH
import com.ortube.constants.WORD_PATH
import com.ortube.constants.enums.ImageMode
import com.ortube.dto.QuizDto
import com.ortube.extensions.MIN_OPTIONS_COUNT
import com.ortube.persistence.domain.enums.AnswerStatus
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class QuizControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return 200 when starting quiz`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        // WHEN
        val startQuizRequest = post(START_QUIZ_ENDPOINT, userPlaylist.id)
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(startQuizRequest)
                .andExpect(jsonPath("$.$QUIZ_ID_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$WORDS_PATH", hasSize<Any>(userPlaylist.words.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return words without images when image mode not specified`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        // WHEN
        val startQuizRequest = post(START_QUIZ_ENDPOINT, userPlaylist.id)
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")


        val startQuizResponse = mockMvc.perform(startQuizRequest)
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andReturn().response.contentAsString

        val (_, words) = objectMapper.readValue(startQuizResponse, QuizDto::class.java)

        // THEN
        assertThat(words.map { it.image }).containsOnlyNulls()
    }

    @Test
    fun `Should return words with images when image mode is with-image`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        // WHEN
        val startQuizRequest = post(START_QUIZ_ENDPOINT, userPlaylist.id).param("image", ImageMode.WITH_IMAGE.toString())
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")


        val startQuizResponse = mockMvc.perform(startQuizRequest)
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andReturn().response.contentAsString

        val (_, words) = objectMapper.readValue(startQuizResponse, QuizDto::class.java)

        // THEN
        assertThat(words.map { it.image }).doesNotContainNull()
    }

    @Test
    fun `Should return 400 when starting quiz and playlist has less than two words`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID, 1)

        // WHEN
        val startQuizRequest = post(START_QUIZ_ENDPOINT, userPlaylist.id)
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(startQuizRequest)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(TOO_FEW_WORDS_MSG))
                .andExpect(status().isBadRequest)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 when getting options of started quiz`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        val startQuizResponse = mockMvc.perform(
                post(START_QUIZ_ENDPOINT, userPlaylist.id)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString

        val (quizId, words) = objectMapper.readValue(startQuizResponse, QuizDto::class.java)

        // WHEN
        val getOptionsRequest = get(GET_QUIZ_OPTIONS_ENDPOINT, quizId, words[0].word)
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(getOptionsRequest) // expects that word has the only one translation, so 4 options should be returned
                .andExpect(jsonPath("$", hasSize<Any>(MIN_OPTIONS_COUNT)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 if quiz not found when getting options`() {
        // GIVEN
        val invalidQuizId = "invalidQuizId"

        // WHEN
        val getOptionsRequest = get(GET_QUIZ_OPTIONS_ENDPOINT, invalidQuizId, "randomWord")
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(getOptionsRequest) // expects that word has the only one translation, so 4 options should be returned
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(QUIZ_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 when user giving an answer`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        val startQuizResponse = mockMvc.perform(
                post(START_QUIZ_ENDPOINT, userPlaylist.id)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken"))
                .andExpect(status().isOk)
                .andReturn()
                .response.contentAsString

        val word = userPlaylist.words[0]
        val (quizId) = objectMapper.readValue(startQuizResponse, QuizDto::class.java)

        val quizWord = word.lang1
        val userAnswer = word.lang2

        // WHEN
        val giveAndAnswerRequest = put(GIVE_QUIZ_ANSWER_ENDPOINT, quizId, quizWord)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(listOf(userAnswer)))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(giveAndAnswerRequest)
                .andExpect(jsonPath("$.$WORD_PATH").value(quizWord))
                .andExpect(jsonPath("$.$ANSWER_STATUS_PATH").value(AnswerStatus.RIGHT.name))
                .andExpect(jsonPath("$.$CORRECT_OPTIONS_PATH", containsInAnyOrder(userAnswer)))
                .andExpect(jsonPath("$.$USER_ANSWERS_PATH", containsInAnyOrder(userAnswer)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 400 when no user answers are present`() {
        // GIVEN
        val emptyList = emptyList<Any>()

        // WHEN
        val giveAndAnswerRequest = put(GIVE_QUIZ_ANSWER_ENDPOINT, randomAlphabetic(10), randomAlphabetic(10))
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emptyList))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(giveAndAnswerRequest)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(TOO_FEW_ANSWERS_MSG))
                .andExpect(status().isBadRequest)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 when finish quiz and no answers provided`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        val startQuizResponse = mockMvc.perform(
                post(START_QUIZ_ENDPOINT, userPlaylist.id)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString

        val (quizId) = objectMapper.readValue(startQuizResponse, QuizDto::class.java)

        // WHEN
        val finishQuizRequest = put("$QUIZ_ENDPOINT$quizId")
                .contentType(APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(finishQuizRequest)
                .andExpect(jsonPath("$.$RESULTS_PATH").value(0))
                .andExpect(jsonPath("$.$ANSWERS_PATH", Matchers.empty<Any>()))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
    }

    @Test
    fun `Should return 400 if quiz mode is invalid`() {
        //GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)
        val quizMode = "invalid"

        // WHEN
        val result = mockMvc.perform(
                post(START_QUIZ_ENDPOINT, userPlaylist.id).param("mode", quizMode)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken"))
        // THEN
        result
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALUE_IS_INVALID.format(quizMode)))
                .andExpect(status().isBadRequest)
    }

    @Test
    fun `Should return 400 if image mode is invalid`() {
        //GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist(USER_ID)
        val imageMode = "invalid"

        // WHEN
        val result = mockMvc.perform(
                post(START_QUIZ_ENDPOINT, userPlaylist.id).param("image", imageMode)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken"))
        // THEN
        result
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALUE_IS_INVALID.format(imageMode)))
                .andExpect(status().isBadRequest)
    }

}