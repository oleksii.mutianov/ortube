package com.ortube.controller

import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.ADD_USER_PLAYLIST_ENDPOINT
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.INVALID_IMAGE_URL
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.MUST_NOT_BE_EMPTY
import com.ortube.constants.MUST_NOT_BE_NULL
import com.ortube.constants.PLAYLIST_NOT_FOUND_MSG
import com.ortube.constants.USER_PLAYLIST_ENDPOINT
import com.ortube.constants.VALIDATION_ERRORS_PATH
import com.ortube.constants.VALIDATION_FAILED_MSG
import com.ortube.constants.YOU_CANNOT_COPY_THIS_PLAYLIST_MSG
import com.ortube.dto.UserPlaylistRequestDto
import com.ortube.persistence.domain.AbstractPlaylist.PlaylistFields.IMAGE
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.PLAYLIST_ID
import com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.PRIVACY
import com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.TITLE
import com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.TOPIC
import com.ortube.persistence.domain.UserPlaylist.UserPlaylistFields.WORDS
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UserPlaylistControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return 200 and empty array when getting all playlists and there are no playlists`() {

        // WHEN
        val requestBuilder = get(USER_PLAYLIST_ENDPOINT)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$", hasSize<Any>(0)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 and array with size as all playlists when getting all playlists`() {

        // GIVEN
        val playlists = listOf(
                userPlaylistDataGenerator.userPlaylist(USER_ID),
                userPlaylistDataGenerator.userPlaylist(USER_ID),
                userPlaylistDataGenerator.userPlaylist(USER_ID)
        )

        // WHEN
        val requestBuilder = get(USER_PLAYLIST_ENDPOINT)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$", hasSize<Any>(playlists.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 response and playlist when getting playlist by id`() {

        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        // WHEN
        val requestBuilder = get("$USER_PLAYLIST_ENDPOINT${playlist.id}")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").value(playlist.id))
                .andExpect(jsonPath("$.$TITLE").value(playlist.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlist.topic))
                .andExpect(jsonPath("$.$PRIVACY").value(playlist.privacy.name))
                .andExpect(jsonPath("$.$WORDS", hasSize<Any>(playlist.words.size)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 when playlist by id not found`() {

        // GIVEN
        val wrongId = "-1"

        // WHEN
        val requestBuilder = get("$USER_PLAYLIST_ENDPOINT$wrongId")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 201 and playlist when creating playlist`() {

        // GIVEN
        val playlistRequestDto = userPlaylistDataGenerator.userPlaylistRequestDto()

        // WHEN
        val requestBuilder = post(USER_PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").isNotEmpty)
                .andExpect(jsonPath("$.$TITLE").value(playlistRequestDto.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlistRequestDto.topic))
                .andExpect(jsonPath("$.$PRIVACY").value(playlistRequestDto.privacy.name))
                .andExpect(status().isCreated)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 422 if payload is invalid`() {
        // GIVEN
        val userPlaylistRequestDto = UserPlaylistRequestDto.builder()
                .title("")
                .image("invalid")
                .build()

        // WHEN
        val requestBuilder = post(USER_PLAYLIST_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userPlaylistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALIDATION_FAILED_MSG))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$TITLE[0]").value(MUST_NOT_BE_EMPTY))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$IMAGE[0]").value(INVALID_IMAGE_URL))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$PRIVACY[0]").value(MUST_NOT_BE_NULL))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))

    }

    @Test
    fun `Should return 200 and update playlist when updating playlist`() {

        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist(USER_ID)
        val playlistRequestDto = userPlaylistDataGenerator.userPlaylistRequestDto()

        // WHEN
        val requestBuilder = put("$USER_PLAYLIST_ENDPOINT${playlist.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").value(playlist.id))
                .andExpect(jsonPath("$.$TITLE").value(playlistRequestDto.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlistRequestDto.topic))
                .andExpect(jsonPath("$.$PRIVACY").value(playlistRequestDto.privacy.name))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 if playlist not found when updating playlist`() {

        // GIVEN
        val playlistRequestDto = userPlaylistDataGenerator.userPlaylistRequestDto()

        // WHEN
        val requestBuilder = put("${USER_PLAYLIST_ENDPOINT}invalidId")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(playlistRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 204 and delete playlist when deleting playlist`() {

        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist(USER_ID)

        // WHEN
        val requestBuilder = delete("$USER_PLAYLIST_ENDPOINT${playlist.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNoContent)

        assertNull(mongoTemplate.findById(playlist.id, UserPlaylist::class.java))
    }

    @Test
    fun `Should return 404 if playlist not found when deleting playlist`() {

        // GIVEN
        val wrongId = "1"

        // WHEN
        val requestBuilder = delete("$USER_PLAYLIST_ENDPOINT$wrongId")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 201 when adding playlist from global playlist list`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        val requestBuilder = post("$ADD_USER_PLAYLIST_ENDPOINT${playlist.id}")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").isNotEmpty)
                .andExpect(jsonPath("$.$TITLE").value(playlist.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlist.topic))
                .andExpect(jsonPath("$.$PRIVACY").value(PlaylistPrivacy.PRIVATE.name))
                .andExpect(status().isCreated)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 201 when adding playlist from other user`() {

        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist(USER_ID, PlaylistPrivacy.PUBLIC)

        // WHEN
        val requestBuilder = post("${USER_PLAYLIST_ENDPOINT}user/$USER_ID/add/${playlist.id}")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$PLAYLIST_ID").isNotEmpty)
                .andExpect(jsonPath("$.$TITLE").value(playlist.title))
                .andExpect(jsonPath("$.$TOPIC").value(playlist.topic))
                .andExpect(jsonPath("$.$PRIVACY").value(PlaylistPrivacy.PRIVATE.name))
                .andExpect(status().isCreated)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 403 when trying to add private playlist from other user`() {

        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist(USER_ID, PlaylistPrivacy.PRIVATE)

        // WHEN
        val requestBuilder = post("${USER_PLAYLIST_ENDPOINT}user/$USER_ID/add/${playlist.id}")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(YOU_CANNOT_COPY_THIS_PLAYLIST_MSG))
                .andExpect(status().isForbidden)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 and users ublic playlists`() {

        // GIVEN
        userPlaylistDataGenerator.userPlaylist(USER_ID, PlaylistPrivacy.PRIVATE)
        userPlaylistDataGenerator.userPlaylist(USER_ID, PlaylistPrivacy.PUBLIC)
        userPlaylistDataGenerator.userPlaylist(USER_ID, PlaylistPrivacy.PUBLIC)

        // WHEN
        val requestBuilder = get("${USER_PLAYLIST_ENDPOINT}user/$USER_ID")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$", hasSize<Any>(2)))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }
}