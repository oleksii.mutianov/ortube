package com.ortube.controller

import com.ortube.config.AbstractSecurityIntegrationTest
import com.ortube.constants.ACCESS_TOKEN_PATH
import com.ortube.constants.AUTHENTICATION_FAILED_MSG
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.BROWSER_FINGERPRINT_HEADER
import com.ortube.constants.EXPIRED_AT_PATH
import com.ortube.constants.FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.INVALID_USERNAME_OR_PASSWORD_MSG
import com.ortube.constants.LOGIN_ENDPOINT
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.ME_ENDPOINT
import com.ortube.constants.MUST_BE_ONLY_REFRESH_TOKEN_MSG
import com.ortube.constants.REFRESH_TOKENS_ENDPOINT
import com.ortube.constants.REFRESH_TOKEN_PATH
import com.ortube.constants.ROLE_PATH
import com.ortube.dto.AuthenticationResponseDto
import com.ortube.dto.LoginRequestDto
import com.ortube.exceptions.JwtAuthenticationException
import com.ortube.persistence.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AuthControllerIT : AbstractSecurityIntegrationTest() {

    @Value("\${jwt.refreshToken.expired}")
    private var refreshTokenExpirationTime: Long = 0

    @Value("\${jwt.token.expired}")
    private var tokenExpirationTime: Long = 0

    @Test
    fun `Should return tokens and user role when successfully login with username`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$ACCESS_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$REFRESH_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$EXPIRED_AT_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$ROLE_PATH").value(testUser.userRole.name))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return tokens and user role when successfully login with email`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.email, PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$ACCESS_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$REFRESH_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$EXPIRED_AT_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$ROLE_PATH").value(testUser.userRole.name))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should save refresh token to DB on success login`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(requestBuilder)
                .andExpect(status().isOk)
                .andReturn()
                .response
                .contentAsString

        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)
        val refreshTokens = mongoTemplate.findById(testUser.id, User::class.java)!!.refreshTokens
        val savedToken = refreshTokens[0]

        // THEN
        assertThat(refreshTokens).hasSize(1)
        assertEquals(authResponse.refreshToken, savedToken.token)
        assertEquals(BROWSER_FINGERPRINT, savedToken.browserFingerprint)
    }

    @Test
    fun `Should replace old refresh token with the same fingerprint on success login`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val oldRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val oldContent = mockMvc.perform(oldRequestBuilder).andReturn().response.contentAsString
        val oldAuthResponse = objectMapper.readValue(oldContent, AuthenticationResponseDto::class.java)

        val newRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val newContent = mockMvc.perform(newRequestBuilder).andReturn().response.contentAsString
        val newAuthResponse = objectMapper.readValue(newContent, AuthenticationResponseDto::class.java)
        val refreshTokens = mongoTemplate.findById(testUser.id, User::class.java)!!.refreshTokens
        val savedToken = refreshTokens[0]

        // THEN
        refreshTokens.forEach { assertNotEquals(oldAuthResponse.refreshToken, it) }
        assertThat(refreshTokens).hasSize(1)
        assertEquals(newAuthResponse.refreshToken, savedToken.token)
        assertEquals(BROWSER_FINGERPRINT, savedToken.browserFingerprint)
    }

    @Test
    fun `Should have multiple refresh tokens with different fingerprints on success login`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val oldRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val oldContent = mockMvc.perform(oldRequestBuilder).andReturn().response.contentAsString
        val oldAuthResponse = objectMapper.readValue(oldContent, AuthenticationResponseDto::class.java)

        val newRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, "Another$BROWSER_FINGERPRINT")

        val newContent = mockMvc.perform(newRequestBuilder).andReturn().response.contentAsString
        val newAuthResponse = objectMapper.readValue(newContent, AuthenticationResponseDto::class.java)
        val refreshTokens = mongoTemplate.findById(testUser.id, User::class.java)!!.refreshTokens

        val tokens = refreshTokens.map { it.token }
        val fingerprints = refreshTokens.map { it.browserFingerprint }

        // THEN
        assertThat(refreshTokens).hasSize(2)
        assertThat(tokens).containsAll(listOf(oldAuthResponse.refreshToken, newAuthResponse.refreshToken))
        assertThat(fingerprints).containsAll(listOf(BROWSER_FINGERPRINT, "Another$BROWSER_FINGERPRINT"))
    }

    @Test
    fun `Should return 401 if username is invalid`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto("Wrong username", PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(INVALID_USERNAME_OR_PASSWORD_MSG))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 401 if password is invalid`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, "Wrong Password")

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(INVALID_USERNAME_OR_PASSWORD_MSG))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 401 if trying to login without fingerprint present in headers`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        // WHEN
        val requestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(FINGERPRINT_HEADER_CANNOT_BE_BLANK_MSG))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should refresh tokens with right credentials`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        val refreshTokensRequestBuilder = get(REFRESH_TOKENS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.refreshToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(refreshTokensRequestBuilder)
                .andExpect(jsonPath("$.$ACCESS_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$EXPIRED_AT_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$REFRESH_TOKEN_PATH").isNotEmpty)
                .andExpect(jsonPath("$.$ROLE_PATH").value(testUser.userRole.name))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should delete old refresh token from DB after successful refresh tokens`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val oldContent = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val oldAuthResponse = objectMapper.readValue(oldContent, AuthenticationResponseDto::class.java)

        // WHEN
        val refreshTokensRequestBuilder = get(REFRESH_TOKENS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${oldAuthResponse.refreshToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val newContent = mockMvc.perform(refreshTokensRequestBuilder).andReturn().response.contentAsString
        val newAuthResponse = objectMapper.readValue(newContent, AuthenticationResponseDto::class.java)
        val refreshTokens = mongoTemplate.findById(testUser.id, User::class.java)!!.refreshTokens
        val savedToken = refreshTokens[0]

        // THEN
        refreshTokens.forEach { assertNotEquals(oldAuthResponse.refreshToken, it) }
        assertThat(refreshTokens).hasSize(1)
        assertEquals(newAuthResponse.refreshToken, savedToken.token)
        assertEquals(BROWSER_FINGERPRINT, savedToken.browserFingerprint)
    }

    @Test
    @DisplayName("Should return 401 if trying to refresh tokens with wrong fingerprint in headers")
    fun `Should return 401 if trying to refresh tokens with wrong fingerprint in headers`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        val refreshTokensRequestBuilder = get(REFRESH_TOKENS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.refreshToken}")
                .header(BROWSER_FINGERPRINT_HEADER, "Wrong fingerprint")

        // THEN
        mockMvc.perform(refreshTokensRequestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(AUTHENTICATION_FAILED_MSG))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 401 if trying to refresh tokens with access token`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        val refreshTokensRequestBuilder = get(REFRESH_TOKENS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.accessToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(refreshTokensRequestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(MUST_BE_ONLY_REFRESH_TOKEN_MSG))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return user account details`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        val getMeRequestBuilder = get(ME_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.accessToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        mockMvc.perform(getMeRequestBuilder)
                .andExpect(jsonPath("$.id").value(testUser.id))
                .andExpect(jsonPath("$.email").value(testUser.email))
                .andExpect(jsonPath("$.role").value(testUser.userRole.name))
                .andExpect(jsonPath("$.username").value(testUser.username))
                .andExpect(jsonPath("$.nickname").value(testUser.nickname))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should throw JwtAuthenticationException if trying to get access with expired access token`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        Thread.sleep(tokenExpirationTime * 1000)
        val getMeRequestBuilder = get(ME_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.accessToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        assertThrows(JwtAuthenticationException::class.java) { mockMvc.perform(getMeRequestBuilder) }
    }

    @Test
    fun `Should throw JwtAuthenticationException if trying to refresh tokens with expired refresh token`() {

        // GIVEN
        val loginRequestDto = LoginRequestDto(testUser.username, PASSWORD)

        val authRequestBuilder = post(LOGIN_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequestDto))
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        val content = mockMvc.perform(authRequestBuilder).andReturn().response.contentAsString
        val authResponse = objectMapper.readValue(content, AuthenticationResponseDto::class.java)

        // WHEN
        Thread.sleep(refreshTokenExpirationTime * 1000)
        val getMeRequestBuilder = get(REFRESH_TOKENS_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX${authResponse.refreshToken}")
                .header(BROWSER_FINGERPRINT_HEADER, BROWSER_FINGERPRINT)

        // THEN
        assertThrows(JwtAuthenticationException::class.java) { mockMvc.perform(getMeRequestBuilder) }
    }
}