package com.ortube.controller

import com.cloudinary.Uploader
import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.FOLDER_PATH
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.IMAGE_LINK_PATH
import com.ortube.constants.IMAGE_UPLOAD_ENDPOINT
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.SIZE_PATH
import com.ortube.constants.UNSUPPORTED_IMAGE_FORMAT_MSG
import com.ortube.constants.VALUE_IS_INVALID
import com.ortube.constants.enums.UploadFolderName
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.mock
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ImageUploadControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return 400 if file has unsupported format`() {
        // GIVEN
        val file = MockMultipartFile("file", "video.mp4", null, "video.mp4".toByteArray())

        // WHEN
        val request = multipart("$IMAGE_UPLOAD_ENDPOINT/${UploadFolderName.playlists}")
                .file(file)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(UNSUPPORTED_IMAGE_FORMAT_MSG))
                .andExpect(status().isBadRequest)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 400 if provided invalid folder name`() {
        // GIVEN
        val file = MockMultipartFile("file", "image.jpg", null, "image.jpg".toByteArray())
        val invalidFolderName = "invalidName"

        // WHEN
        val request = multipart("$IMAGE_UPLOAD_ENDPOINT/$invalidFolderName")
                .file(file)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALUE_IS_INVALID.format(invalidFolderName)))
                .andExpect(status().isBadRequest)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should upload image`() {
        // GIVEN
        val fileName = "image.jpg"
        val fileBytes = fileName.toByteArray()
        val file = MockMultipartFile("file", fileName, null, fileBytes)

        val expectedUrl = randomAlphabetic(10)
        val folder = UploadFolderName.playlists

        given(cloudinary.uploader()).willReturn(mock(Uploader::class.java))
        given(cloudinary.uploader().upload(file.bytes, mapOf(Pair("folder", folder.name)))).willReturn(mapOf(Pair("url", expectedUrl)))

        // WHEN
        val request = multipart("$IMAGE_UPLOAD_ENDPOINT/$folder")
                .file(file)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.$IMAGE_LINK_PATH").value(expectedUrl))
                .andExpect(jsonPath("$.$FOLDER_PATH").value(folder.name))
                .andExpect(jsonPath("$.$SIZE_PATH").value(fileBytes.size))
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

}