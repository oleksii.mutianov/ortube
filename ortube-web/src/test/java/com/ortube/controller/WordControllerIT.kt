package com.ortube.controller

import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.INVALID_IMAGE_URL
import com.ortube.constants.MESSAGE_PATH
import com.ortube.constants.MUST_NOT_BE_EMPTY
import com.ortube.constants.PLAYLIST_NOT_FOUND_MSG
import com.ortube.constants.SECURE_PLAYLIST_ENDPOINT
import com.ortube.constants.VALIDATION_ERRORS_PATH
import com.ortube.constants.VALIDATION_FAILED_MSG
import com.ortube.constants.WORD_ENDPOINT
import com.ortube.dto.WordRequestDto
import com.ortube.persistence.domain.Word.WordFields.IMAGE
import com.ortube.persistence.domain.Word.WordFields.LANG_1
import com.ortube.persistence.domain.Word.WordFields.LANG_2
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class WordControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return 201 when adding word to playlist`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val wordRequestDto = wordDataGenerator.wordRequestDto()

        // WHEN
        val requestBuilder = post("$SECURE_PLAYLIST_ENDPOINT${playlist.id}$WORD_ENDPOINT")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 422 if payload is invalid`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val wordRequestDto = WordRequestDto(null, null, "invalid")

        // WHEN
        val requestBuilder = post("$SECURE_PLAYLIST_ENDPOINT${playlist.id}$WORD_ENDPOINT")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(VALIDATION_FAILED_MSG))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$LANG_1[0]").value(MUST_NOT_BE_EMPTY))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$LANG_2[0]").value(MUST_NOT_BE_EMPTY))
                .andExpect(jsonPath("$.$VALIDATION_ERRORS_PATH.$IMAGE[0]").value(INVALID_IMAGE_URL))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 if playlist not found when adding word to playlist`() {

        // GIVEN
        val wordRequestDto = wordDataGenerator.wordRequestDto()

        // WHEN
        val requestBuilder = post("${SECURE_PLAYLIST_ENDPOINT}invalidId$WORD_ENDPOINT")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.$MESSAGE_PATH").value(PLAYLIST_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 when updating word`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val originalWord = playlist.words[0]
        val wordRequestDto = wordDataGenerator.wordRequestDto()

        // WHEN
        val requestBuilder = put("$SECURE_PLAYLIST_ENDPOINT${playlist.id}$WORD_ENDPOINT${originalWord.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordRequestDto))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 200 when deleting one word`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val originalWord = playlist.words[0]

        // WHEN
        val requestBuilder = delete("$SECURE_PLAYLIST_ENDPOINT${playlist.id}$WORD_ENDPOINT${originalWord.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNoContent)
    }

    @Test
    fun `Should return 200 when deleting multiple words`() {

        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val wordIds = playlist.words.map { it.id }

        // WHEN
        val requestBuilder = delete("$SECURE_PLAYLIST_ENDPOINT${playlist.id}$WORD_ENDPOINT")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordIds))
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$adminAccessToken")

        // THEN
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNoContent)
    }
}