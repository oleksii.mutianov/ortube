package com.ortube.datagenerators

import com.ortube.dto.WordRequestDto
import com.ortube.persistence.domain.Word
import org.apache.commons.lang3.RandomStringUtils.random
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import randomWordsImageUrl

@Component
@Profile("integration-test")
class WordDataGenerator {

    fun word() = Word(
            lang1 = randomAlphabetic(30),
            lang2 = random(30),
            image = randomWordsImageUrl()
    )

    fun words(size: Int = 3) = generateSequence { word() }.take(size).toMutableList()

    fun wordRequestDto() = WordRequestDto(
            lang1 = randomAlphabetic(30),
            lang2 = random(30),
            image = randomWordsImageUrl()
    )

}