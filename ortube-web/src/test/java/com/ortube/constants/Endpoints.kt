package com.ortube.constants

const val PLAYLIST_ENDPOINT = "/playlist"
const val SECURE_PLAYLIST_ENDPOINT = "/secure/playlist/"
const val WORD_ENDPOINT = "/word/"

const val LOGIN_ENDPOINT = "/auth/login"
const val REFRESH_TOKENS_ENDPOINT = "/auth/token"
const val ME_ENDPOINT = "/auth/me"

const val USER_PLAYLIST_ENDPOINT = "/secure/userPlaylist/"
const val ADD_USER_PLAYLIST_ENDPOINT = "$USER_PLAYLIST_ENDPOINT/add/"

const val REGISTER_ENDPOINT = "/register"

const val QUIZ_ENDPOINT = "/secure/quiz/"
const val START_QUIZ_ENDPOINT = "$QUIZ_ENDPOINT/start/{userPlaylistId}"
const val GET_QUIZ_OPTIONS_ENDPOINT = "$QUIZ_ENDPOINT{quizId}/words/{wordTitle}"
const val GIVE_QUIZ_ANSWER_ENDPOINT = GET_QUIZ_OPTIONS_ENDPOINT

const val IMAGE_UPLOAD_ENDPOINT = "/image"
