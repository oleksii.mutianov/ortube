package com.ortube.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.ortube.datagenerators.UserDataGenerator
import com.ortube.mail.service.MailSender
import com.ortube.persistence.domain.User
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
abstract class AbstractSecurityIntegrationTest {

    companion object {
        const val BROWSER_FINGERPRINT = "JavaSecurityTest"
        const val PASSWORD = "Password"
        val ENCODED_PASSWORD = BCryptPasswordEncoder().encode(PASSWORD)!!
    }

    lateinit var testUser: User

    @Autowired
    lateinit var mongoTemplate: MongoTemplate

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var userDataGenerator: UserDataGenerator

    @MockBean
    lateinit var mailService: MailSender

    @BeforeEach
    fun createUser() {
        testUser = userDataGenerator.mockUser().apply { password = ENCODED_PASSWORD }
        mongoTemplate.save(testUser)
    }

    @AfterEach
    fun clearDB() {
        mongoTemplate.dropCollection(User::class.java)
    }

}