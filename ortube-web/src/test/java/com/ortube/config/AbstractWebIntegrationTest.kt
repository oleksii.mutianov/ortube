package com.ortube.config

import com.cloudinary.Cloudinary
import com.fasterxml.jackson.databind.ObjectMapper
import com.ortube.datagenerators.PlaylistDataGenerator
import com.ortube.datagenerators.UserPlaylistDataGenerator
import com.ortube.datagenerators.WordDataGenerator
import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.domain.Quiz
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.UserRole
import com.ortube.persistence.repository.UserRepository
import com.ortube.security.jwt.JwtUtils
import com.ortube.security.model.TokenScopes
import io.jsonwebtoken.Jwts
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import java.time.LocalDateTime
import java.util.Optional

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
abstract class AbstractWebIntegrationTest {

    @Autowired
    lateinit var mongoTemplate: MongoTemplate

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var playlistDataGenerator: PlaylistDataGenerator

    @Autowired
    lateinit var userPlaylistDataGenerator: UserPlaylistDataGenerator

    @Autowired
    lateinit var wordDataGenerator: WordDataGenerator

    @MockBean
    lateinit var userRepository: UserRepository

    @MockBean
    lateinit var javaMailSender: JavaMailSender

    @MockBean
    lateinit var cloudinary: Cloudinary

    @Value("\${jwt.token.secret}")
    var tokenTestSigningKey: String = ""

    @Value("\${jwt.refreshToken.expired}")
    var tokenTestExpirationTime: Long = 0

    companion object {
        const val USER_ID = "2"
        const val USER_USERNAME = "User"
        const val ADMIN_USERNAME = "Admin"

        private const val USER_EMAIL = "Email"
        private const val ADMIN_ID = "1"
        private const val ADMIN_EMAIL = "Email_Admin"
        private const val PASSWORD = "Password"

        private val encoder = BCryptPasswordEncoder()

        var adminAccessToken: String? = null
        var userAccessToken: String? = null

        lateinit var admin: User
        lateinit var user: User

        @JvmStatic
        @BeforeAll
        fun createUsers() {

            admin = User.builder()
                    .id(ADMIN_ID)
                    .username(ADMIN_USERNAME)
                    .password(encoder.encode(PASSWORD))
                    .email(ADMIN_EMAIL)
                    .userRole(UserRole.ADMIN)
                    .build()

            user = User.builder()
                    .id(USER_ID)
                    .username(USER_USERNAME)
                    .password(encoder.encode(PASSWORD))
                    .email(USER_EMAIL)
                    .userRole(UserRole.USER)
                    .build()
        }
    }

    @BeforeEach
    fun mockUserAndCreateToken() {
        Mockito.`when`(userRepository.findByEmailOrUsername(ADMIN_USERNAME, ADMIN_USERNAME)).thenReturn(Optional.of(admin))
        Mockito.`when`(userRepository.findByEmailOrUsername(USER_USERNAME, USER_USERNAME)).thenReturn(Optional.of(user))
        adminAccessToken = initAdminToken()
        userAccessToken = initUserToken()
    }

    @AfterEach
    fun clearDB() {
        mongoTemplate.dropCollection(Playlist::class.java)
        mongoTemplate.dropCollection(UserPlaylist::class.java)
        mongoTemplate.dropCollection(Quiz::class.java)
    }

    private fun initAdminToken() = initToken(admin.username)

    private fun initUserToken() = initToken(user.username)

    private fun initToken(subject: String): String {
        val claims = Jwts.claims().setSubject(subject).apply { this["scope"] = TokenScopes.ACCESS_TOKEN.value }
        val currentTime = LocalDateTime.now()

        return JwtUtils.build(claims, currentTime, tokenTestExpirationTime, tokenTestSigningKey)
    }

}