package com.ortube.mail.service.impl

import com.ortube.mail.data.MailDto
import com.ortube.mail.service.MailSender
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets

@Service
class MailSenderImpl(
    private val emailSender: JavaMailSender
) : MailSender {

    override fun sendEmail(mailDto: MailDto) = emailSender.send(prepareMessage(mailDto))

    private fun prepareMessage(mailDto: MailDto) =
            emailSender.createMimeMessage().apply {
                setContent(mailDto.body, "text/html")
                MimeMessageHelper(this, StandardCharsets.UTF_8.name()).apply {
                    setTo(mailDto.sendTo)
                    setSubject(mailDto.subject)
                    setText(mailDto.body, true)
                }
            }

}