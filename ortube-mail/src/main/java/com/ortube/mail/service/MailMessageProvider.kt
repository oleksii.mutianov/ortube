package com.ortube.mail.service

interface MailMessageProvider {
    fun provideMessageBody(templateName: String, properties: Map<String, Any>): String
}
