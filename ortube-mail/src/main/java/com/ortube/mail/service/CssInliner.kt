package com.ortube.mail.service

interface CssInliner {
    fun inlineCss(html: String): String
}