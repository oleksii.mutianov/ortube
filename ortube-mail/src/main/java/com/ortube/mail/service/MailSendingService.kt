package com.ortube.mail.service

interface MailSendingService {

    fun sendActivationEmail(email: String, nickname: String, activationCode: String)

}