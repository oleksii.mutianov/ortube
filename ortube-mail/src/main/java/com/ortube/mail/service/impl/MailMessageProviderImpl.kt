package com.ortube.mail.service.impl

import com.ortube.mail.service.CssInliner
import com.ortube.mail.service.MailMessageProvider
import freemarker.template.Configuration
import org.springframework.stereotype.Service
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils

@Service
class MailMessageProviderImpl(
    private val freemarkerConfig: Configuration,
    private val cssInliner: CssInliner
) : MailMessageProvider {

    override fun provideMessageBody(templateName: String, properties: Map<String, Any>): String {
        val template = freemarkerConfig.getTemplate(templateName)
        val templateString = FreeMarkerTemplateUtils.processTemplateIntoString(template, properties)
        return cssInliner.inlineCss(templateString)
    }

}