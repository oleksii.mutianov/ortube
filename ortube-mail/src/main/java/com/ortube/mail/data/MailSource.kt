package com.ortube.mail.data

import org.fit.cssbox.io.DocumentSource

class MailSource(private val body: String) : DocumentSource(null) {

    override fun getURL() = TODO("Unused")

    override fun close() = TODO("Unused")

    override fun getContentType() = "text/html; charset=utf-8"

    override fun getInputStream() = body.byteInputStream()

}