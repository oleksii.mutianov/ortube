db.user.insert({
    "email": "eml",
    "username": "john.doe",
    "password": "$2a$10$JL/7TEverbHSEEzSx.OqqerPMdNpVHhzAGLQ3tVRy.2qXYwNfnXRK", // password: 123
    "userRole": "ADMIN",
    "nickname": "John Doe",
    "_class": "com.ortube.data.access.domain.User"
})